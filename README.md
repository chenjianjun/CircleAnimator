#CircleAnimator
### 介绍
- 为多圆形进度交互动画(进度条流动动画、文字颜色和背景颜色渐变动画、圆伸缩动画、圆位移动画)
- 可通过ProgressBarController.java控制动画持续时间、大小圆大小和内部组件颜色
- 默认布局可在布局文件设置
- 动态数据可通过setData设置

### 使用方法
- 可通过控制器随意改变属性
```
public class ProgressBarController {
    public static final float DEFAULT_VALUE = -1.111111f;//默认值，若为默认值则显示时为“- -”
    public static final int FLEX_DURATION = 500;// 伸缩动画持续时间
    public static final int FLEX_DURATION_RIGHT = 800;// 伸缩动画持续时间
    public static final int PB_DURATION = 500;// 动画持续时间

    public static final int SCREEN_OFFSET = 720;//参考屏幕分辨率宽度
    public static final int SWEEP_ARC_ANGLE = 270;// 圆弧划过的角度(圆弧最大角度)
    public static final int SWEEP_ARC_ANGLE_START = 135;//默认圆弧的开始角度
    public static final int SWEEP_CIRCLE_ANGLE = -360;// 逆时针旋转的角度
    public static final float BIG_CIRCLE_SCALE = 306.0f;//720屏宽下的大圆宽高
    public static final float SMALL_CIRCLE_SCALE = 113.0f;//720屏宽下的小圆宽高
    public static final int BIG_CIRCLE_STROKE = 14;//大圆外边距
    public static final int BIG_CIRCLE_INTERVAL = 36;//大圆圆弧和内圆的间隔
    public static final int BIG_CIRCLE_INNER_CIRCLE_SCALE = 266;//大圆内部圆直径

    public static final int TITLE_TS_SMALL = 5;//小圆的宽度与小圆标题的文字大小比
    public static final int TITLE_TS_BIG = 10;//大圆的宽度与大圆标题的文字大小比
    public static final int TEXT_U_TS_SMALL = 8;//小圆的宽度与小圆内容单位的文字大小比
    public static final int TEXT_U_TS_BIG = 16;//大圆的宽度与大圆内容单位的文字大小比
    public static final int TEXT_TS = 4;//圆的宽度与圆内容的文字大小比
    public static final int PERCENT_TS = 10;//圆的宽度与圆百分比的文字大小比

    public static final int DEFAULT_FONT_COLOR_BIG = R.color._ffffff;//大圆内部圆文字颜色
    public static final int DEFAULT_INNER_BACKGROUND_COLOR_BIG = R.color._fe9602;//大圆内部圆背景默认颜色
    public static final int OUT_INNER_BACKGROUND_COLOR_BIG = R.color._e73330;//大圆超出总进度的内部圆背景颜色
    public static final int CURR_EXTERNAL_COLOR_BIG = R.color._ff9025;//大圆当前进度条颜色
    public static final int DEFAULT_EXTERNAL_COLOR_BIG = R.color._d8d8d8;//大圆默认进度条颜色
    public static final int OUT_EXTERNAL_COLOR_BIG = R.color._e83431;//大圆超出总进度的进度条颜色
    public static final int DEFAULT_INNER_BACKGROUND_COLOR_SMALL = R.color._fec36f;//小圆内部圆背景默认颜色
    public static final int DEFAULT_FONT_COLOR_SMALL = R.color._ff9025;//小圆内部圆文字颜色
    public static final int CURR_EXTERNAL_COLOR_SMALL = R.color._fec36f;//小圆默认进度条颜色
    public static final int OUT_INNER_BACKGROUND_COLOR_SMALL = R.color._e56866;//小圆超出总进度的内部圆背景颜色
    public static final int OUT_FONT_COLOR_BIG = R.color._e73330;//超出总进度小圆内部圆文字颜色
    public static final int OUT_EXTERNAL_COLOR_SMALL = R.color._fd4456;//小圆超出总进度的进度条颜色

    public static final int TITLE_FONT_COUNT = 2;//标题简写保留前几位
```
- 可在布局文件中设置进度圆的属性
```
<RelativeLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:pb_circle="http://schemas.android.com/apk/res/com.example.circleanimation"
    android:id="@+id/ll"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@android:color/white"
    android:orientation="vertical"
    >
    <com.example.circleanimation.MultiplePb
        android:id="@+id/multiplepb"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_below="@id/tv"
        android:layout_marginTop="20dp"
        android:background="@android:color/white"
        pb_circle:odd_pbcLeftBottom="-1.111111"
        pb_circle:odd_pbcLeftMiddle="-1.111111"
        pb_circle:odd_pbcLeftTop="-1.111111"
        pb_circle:odd_pbcMiddle="-1.111111"
        pb_circle:odd_pbcRightBottom="-1.111111"
        pb_circle:odd_pbcRightMiddle="-1.111111"
        pb_circle:odd_pbcRightTop="-1.111111"
        pb_circle:s_u_pbcLeftBottom="U"
        pb_circle:s_u_pbcLeftMiddle="U"
        pb_circle:s_u_pbcLeftTop="U"
        pb_circle:s_u_pbcMiddle="U"
        pb_circle:s_u_pbcRightBottom="U"
        pb_circle:s_u_pbcRightMiddle="U"
        pb_circle:s_u_pbcRightTop="U"
        pb_circle:title_pbcLeftBottom="Title"
        pb_circle:title_pbcLeftMiddle="Title"
        pb_circle:title_pbcLeftTop="Title"
        pb_circle:title_pbcMiddle="Title"
        pb_circle:title_pbcRightBottom="Title"
        pb_circle:title_pbcRightMiddle="Title"
        pb_circle:title_pbcRightTop="Title"
        pb_circle:total_pbcLeftBottom="-1.111111"
        pb_circle:total_pbcLeftMiddle="-1.111111"
        pb_circle:total_pbcLeftTop="-1.111111"
        pb_circle:total_pbcMiddle="-1.111111"
        pb_circle:total_pbcRightBottom="-1.111111"
        pb_circle:total_pbcRightMiddle="-1.111111"
        pb_circle:total_pbcRightTop="-1.111111"
        pb_circle:unit_pbcLeftBottom="Unit"
        pb_circle:unit_pbcLeftMiddle="Unit"
        pb_circle:unit_pbcLeftTop="Unit"
        pb_circle:unit_pbcMiddle="Unit"
        pb_circle:unit_pbcRightBottom="Unit"
        pb_circle:unit_pbcRightMiddle="Unit"
        pb_circle:unit_pbcRightTop="Unit" />

</RelativeLayout>

```
各个属性可在attrs.xml中找到。下面选右上小圆举例，其他同理。
1. pb_circle:unit_pbcRightTop对应右上小圆的详细单位
2. pb_circle:s_u_pbcRightTop对应右上小圆的缩写单位
3. pb_circle:total_pbcRightTop对应右上小圆的总量
4. pb_circle:odd_pbcRightTop对应右上小圆的剩余量
5. pb_circle:title_pbcRightTop对应右上小圆的标题
### 动画效果

![动态图](http://jianjunchen.net/wp-content/uploads/2015/09/circle-1.gif)

**注意：该项目采用了GPL v2许可证,请勿用于商业二次发布。
详情可查看LICENSE**