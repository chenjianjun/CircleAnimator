package com.example.circleanimation.model;

/**
 * 进度控件model
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class PBCInfo implements Cloneable {
    private int style;//类型  1:左上控件  2:左中控件  3:左下控件  4:中间控件  5:右上控件  6:右中控件  7:右下控件
    private String title = "";//标题
    private float odd;//剩余次数
    private float total;//总次数
    private String unit = "";//单位
    private String s_u = "";//缩写单位
    private boolean isFloat;//内容是否支持浮点类型,默认不支持

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getOdd() {
        return odd;
    }

    public void setOdd(float odd) {
        this.odd = odd;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public int getStyle() {
        return style;
    }

    public void setStyle(int style) {
        this.style = style;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getS_u() {
        return s_u;
    }

    public void setS_u(String s_u) {
        this.s_u = s_u;
    }

    public boolean getIsFloat() {
        return isFloat;
    }

    public void setIsFloat(boolean isFloat) {
        this.isFloat = isFloat;
    }
}
