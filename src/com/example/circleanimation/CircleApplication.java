package com.example.circleanimation;

import android.app.Application;

/**
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class CircleApplication extends Application {

    private static CircleApplication application;
    private int wid;

    public static CircleApplication getInstance() {
        return application;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }

    public int getWid() {
        return wid;
    }

    public void setWid(int wid) {
        this.wid = wid;
    }
}
