package com.example.circleanimation.model;

/**
 * 动画数据集
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class PBCMultiple {
    private PBCInfo l_t_pbcInfo;//左上小圆数据
    private PBCInfo l_m_pbcInfo;//左中小圆数据
    private PBCInfo l_b_pbcInfo;//左下小圆数据
    private PBCInfo m_pbcInfo;//中间大圆数据
    private PBCInfo r_t_pbcInfo;//右上小圆数据
    private PBCInfo r_m_pbcInfo;//右中小圆数据
    private PBCInfo r_b_pbcInfo;//右下小圆数据

    public PBCInfo getL_b_pbcInfo() {
        return l_b_pbcInfo;
    }

    public void setL_b_pbcInfo(PBCInfo l_b_pbcInfo) {
        this.l_b_pbcInfo = l_b_pbcInfo;
    }

    public PBCInfo getL_m_pbcInfo() {
        return l_m_pbcInfo;
    }

    public void setL_m_pbcInfo(PBCInfo l_m_pbcInfo) {
        this.l_m_pbcInfo = l_m_pbcInfo;
    }

    public PBCInfo getL_t_pbcInfo() {
        return l_t_pbcInfo;
    }

    public void setL_t_pbcInfo(PBCInfo l_t_pbcInfo) {
        this.l_t_pbcInfo = l_t_pbcInfo;
    }

    public PBCInfo getM_pbcInfo() {
        return m_pbcInfo;
    }

    public void setM_pbcInfo(PBCInfo m_pbcInfo) {
        this.m_pbcInfo = m_pbcInfo;
    }

    public PBCInfo getR_b_pbcInfo() {
        return r_b_pbcInfo;
    }

    public void setR_b_pbcInfo(PBCInfo r_b_pbcInfo) {
        this.r_b_pbcInfo = r_b_pbcInfo;
    }

    public PBCInfo getR_m_pbcInfo() {
        return r_m_pbcInfo;
    }

    public void setR_m_pbcInfo(PBCInfo r_m_pbcInfo) {
        this.r_m_pbcInfo = r_m_pbcInfo;
    }

    public PBCInfo getR_t_pbcInfo() {
        return r_t_pbcInfo;
    }

    public void setR_t_pbcInfo(PBCInfo r_t_pbcInfo) {
        this.r_t_pbcInfo = r_t_pbcInfo;
    }

}
