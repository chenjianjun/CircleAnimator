package com.example.circleanimation;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.circleanimation.listener.FunClickListener;
import com.example.circleanimation.model.PBCInfo;
import com.example.circleanimation.model.PBCMultiple;

/**
 * 首页
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class MainActivity extends Activity implements View.OnClickListener, FunClickListener {

    private MultiplePb multiplePb = null;
    private PBCMultiple pbcMultiple = null;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWindowWidth();
        setContentView(R.layout.activity_main);
        multiplePb = (MultiplePb) findViewById(R.id.multiplepb);
        multiplePb.setFunClickListener(this);
        tv = (TextView) findViewById(R.id.tv);
        Button btn = (Button) findViewById(R.id.btn);
        btn.setOnClickListener(this);

        initData();
//        btn.performClick();
    }

    private void initData() {
        pbcMultiple = new PBCMultiple();
        PBCInfo pbcInfoDepthSquats = new PBCInfo();
        pbcInfoDepthSquats.setStyle(1);
        pbcInfoDepthSquats.setTitle("深蹲");
        pbcInfoDepthSquats.setOdd(100);
        pbcInfoDepthSquats.setTotal(500);
        pbcInfoDepthSquats.setS_u("T");//times
        pbcInfoDepthSquats.setUnit("Times");
        pbcMultiple.setL_t_pbcInfo(pbcInfoDepthSquats);
        PBCInfo pbcInfoSwimming = new PBCInfo();
        pbcInfoSwimming.setStyle(2);
        pbcInfoSwimming.setTitle("游泳");
        pbcInfoSwimming.setOdd(400);
        pbcInfoSwimming.setTotal(1000);
        pbcInfoSwimming.setS_u("M");//meters
        pbcInfoSwimming.setUnit("Meters");
        pbcMultiple.setL_m_pbcInfo(pbcInfoSwimming);
        PBCInfo pbcInfoWalking = new PBCInfo();
        pbcInfoWalking.setStyle(3);
        pbcInfoWalking.setTitle("走路");
        pbcInfoWalking.setOdd(-1000);//11000，所以剩余－1000步
        pbcInfoWalking.setTotal(10000);//目标1000步
        pbcInfoWalking.setS_u("S");//steps
        pbcInfoWalking.setUnit("Steps");
        pbcMultiple.setL_b_pbcInfo(pbcInfoWalking);
        PBCInfo pbcInfoJogging = new PBCInfo();
        pbcInfoJogging.setStyle(4);
        pbcInfoJogging.setTitle("跑步");
        pbcInfoJogging.setOdd(4800);
        pbcInfoJogging.setTotal(5000);
        pbcInfoJogging.setS_u("S");//steps
        pbcInfoJogging.setUnit("Steps");
        pbcMultiple.setM_pbcInfo(pbcInfoJogging);
        PBCInfo pbcInfoPushUp = new PBCInfo();
        pbcInfoPushUp.setStyle(5);
        pbcInfoPushUp.setTitle("跳水");
        pbcInfoPushUp.setOdd(10);
        pbcInfoPushUp.setTotal(50);
        pbcInfoPushUp.setS_u("M");//times
        pbcInfoPushUp.setUnit("Meters");
        pbcMultiple.setR_t_pbcInfo(pbcInfoPushUp);
        PBCInfo pbcInfoRopeSkipping = new PBCInfo();
        pbcInfoRopeSkipping.setStyle(6);
        pbcInfoRopeSkipping.setTitle("跳绳");
        pbcInfoRopeSkipping.setOdd(200);
        pbcInfoRopeSkipping.setTotal(300);
        pbcInfoRopeSkipping.setS_u("T");//times
        pbcInfoRopeSkipping.setUnit("Times");
        pbcMultiple.setR_m_pbcInfo(pbcInfoRopeSkipping);
        PBCInfo pbcInfoChinUps = new PBCInfo();
        pbcInfoChinUps.setStyle(7);
        pbcInfoChinUps.setTitle("骑行");
        pbcInfoChinUps.setOdd(100);
        pbcInfoChinUps.setTotal(100);
        pbcInfoChinUps.setS_u("Km");//
        pbcInfoChinUps.setUnit("Kilometer");
        pbcMultiple.setR_b_pbcInfo(pbcInfoChinUps);
        pbcInfoDepthSquats = null;
        pbcInfoSwimming = null;
        pbcInfoWalking = null;
        pbcInfoJogging = null;
        pbcInfoPushUp = null;
        pbcInfoRopeSkipping = null;
        pbcInfoChinUps = null;
    }

    @Override
    public void onClick(View v) {
        multiplePb.setLeftFunText("我的");
        multiplePb.setRightFunText("计划");
        multiplePb.setData(pbcMultiple);
        multiplePb.start(true);
        tv.setText("点击小圆触发动画，点击大圆跳转页面,点击功能按钮跳转页面");
//        multiplePb.performClicked();
    }

    /**
     * 获取屏幕宽度
     */
    public void setWindowWidth() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        CircleApplication.getInstance().setWid(displayMetrics.widthPixels);
    }

    @Override
    public void left_fun_click() {
        Intent i = new Intent(MainActivity.this, DemoActivity.class);
        i.putExtra("content", "左功能按钮跳转");
        startActivity(i);
    }

    @Override
    public void right_fun_click() {
        Intent i = new Intent(MainActivity.this, DemoActivity.class);
        i.putExtra("content", "右功能按钮跳转");
        startActivity(i);
    }

    @Override
    public void big_circle_click(PBCInfo pbcInfo) {
        if (null == pbcInfo) {
            Toast.makeText(CircleApplication.getInstance(), "大圆没数据", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent i = new Intent(MainActivity.this, DemoActivity.class);
        i.putExtra("content", pbcInfo.getTitle() + "大圆跳转");
        startActivity(i);
    }

}
