package com.example.circleanimation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.circleanimation.listener.FunClickListener;
import com.example.circleanimation.listener.ProgressBarListener;
import com.example.circleanimation.model.PBCClickInfo;
import com.example.circleanimation.model.PBCInfo;
import com.example.circleanimation.model.PBCMultiple;
import com.example.circleanimation.util.LogEx;
import com.example.circleanimation.util.ProgressBarController;
import com.example.circleanimation.widget.ProgressBarCircle;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.AnimatorSet;

/**
 * 进度控件集
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class MultiplePb extends FrameLayout implements OnClickListener, ProgressBarListener {
    private static final String TAG = "MultiplePb";

    private static int FLEX_OFFSET = 0;
    private static int MARGIN_LEFT = 0;
    private static int MARGIN_TOP = 0;
    private PBCMultiple pbcMultiple = null;
    private int widthSmall;
    private int widthBig = 0;
    private int screenWidth;
    private FunClickListener funClickListener = null;

    private ProgressBarCircle pb_left_top, pb_left_middle, pb_left_bottom, pb_middle, pb_right_top, pb_right_middle, pb_right_bottom;

    private PbClickListener pbClickListener = null;

    private TextView tv_fun_left, tv_fun_right;

    public MultiplePb(Context context) {
        this(context, null);
    }

    public MultiplePb(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MultiplePb(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        FLEX_OFFSET = getResources().getDimensionPixelSize(R.dimen.dimen_8);
        screenWidth = CircleApplication.getInstance().getWid();
        widthSmall = screenWidth * 113 / ProgressBarController.SCREEN_OFFSET;//720宽屏下小圆宽为113dp
        LogEx.d(TAG, "init：" + context + ";widthSmall:" + widthSmall);
        pbClickListener = new PbClickListener();
        initView(context);
        initData(context, attrs);
        start(true);
    }

    private void initView(Context context) {
        View mainView = inflate(context, R.layout.mul_pb, null);
        removeAllViews();
        addView(mainView);
        pb_left_top = (ProgressBarCircle) mainView.findViewById(R.id.pb_left_top);
        pb_left_middle = (ProgressBarCircle) mainView.findViewById(R.id.pb_left_middle);
        pb_left_bottom = (ProgressBarCircle) mainView.findViewById(R.id.pb_left_bottom);
        pb_middle = (ProgressBarCircle) mainView.findViewById(R.id.pb_middle);
        pb_right_top = (ProgressBarCircle) mainView.findViewById(R.id.pb_right_top);
        pb_right_middle = (ProgressBarCircle) mainView.findViewById(R.id.pb_right_middle);
        pb_right_bottom = (ProgressBarCircle) mainView.findViewById(R.id.pb_right_bottom);

        RelativeLayout fun_layout = (RelativeLayout) mainView.findViewById(R.id.fun_layout);
        tv_fun_left = ((TextView) mainView.findViewById(R.id.tv_fun_left));//我的(左功能按钮)
        tv_fun_left.setOnClickListener(this);
        tv_fun_right = ((TextView) mainView.findViewById(R.id.tv_fun_right));//计划(右功能按钮)
        tv_fun_right.setOnClickListener(this);

        // (除以2==dp) 以720宽分辨率与屏幕的比例为参考得到各大小
        int top_bottom_small_horizontal = screenWidth * 70 / ProgressBarController.SCREEN_OFFSET;//上下小圆的左右边距
        int middle_small_horizontal = screenWidth * 36 / ProgressBarController.SCREEN_OFFSET;//中小圆的左边距
        int fun_left = screenWidth * 206 / ProgressBarController.SCREEN_OFFSET;//功能按钮父布局左对齐
        int top_small_top = screenWidth * 0 / ProgressBarController.SCREEN_OFFSET;//上小圆上边距
        int middle_small_top = screenWidth * 140 / ProgressBarController.SCREEN_OFFSET;//中小圆上边距
        int bottom_small_top = screenWidth * 280 / ProgressBarController.SCREEN_OFFSET;//底小圆上边距
        int big_top = screenWidth * 40 / ProgressBarController.SCREEN_OFFSET;//大圆上边距
        int top_fun = screenWidth * 330 / ProgressBarController.SCREEN_OFFSET;//功能按钮父布局上对齐
        int fun_right_right = screenWidth * 42 / ProgressBarController.SCREEN_OFFSET;//右功能按钮的右边距
        int fun_left_left = fun_right_right;//左功能按钮的左边距
        int w_fun = screenWidth * 308 / ProgressBarController.SCREEN_OFFSET;//功能按钮父布局宽度
        // 左上小圆布局
        RelativeLayout.LayoutParams lp_small_left_top = (RelativeLayout.LayoutParams) pb_left_top.getLayoutParams();
        lp_small_left_top.leftMargin = top_bottom_small_horizontal;
        lp_small_left_top.topMargin = top_small_top;
        lp_small_left_top.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        pb_left_top.setLayoutParams(lp_small_left_top);
        //左中小圆布局
        RelativeLayout.LayoutParams lp_small_left_middle = (RelativeLayout.LayoutParams) pb_left_middle.getLayoutParams();
        lp_small_left_middle.leftMargin = middle_small_horizontal;
        lp_small_left_middle.topMargin = middle_small_top;
        lp_small_left_middle.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        pb_left_middle.setLayoutParams(lp_small_left_middle);
        //左下小圆布局
        RelativeLayout.LayoutParams lp_small_left_bottom = (RelativeLayout.LayoutParams) pb_left_bottom.getLayoutParams();
        lp_small_left_bottom.leftMargin = top_bottom_small_horizontal;
        lp_small_left_bottom.topMargin = bottom_small_top;
        lp_small_left_bottom.addRule(RelativeLayout.ALIGN_PARENT_LEFT, RelativeLayout.TRUE);
        pb_left_bottom.setLayoutParams(lp_small_left_bottom);
        //大圆布局
        RelativeLayout.LayoutParams lp_big_middle = (RelativeLayout.LayoutParams) pb_middle.getLayoutParams();
        lp_big_middle.topMargin = big_top;
        lp_big_middle.addRule(RelativeLayout.CENTER_HORIZONTAL, RelativeLayout.TRUE);
        pb_middle.setLayoutParams(lp_big_middle);
        // 右上小圆布局
        RelativeLayout.LayoutParams lp_small_right_top = (RelativeLayout.LayoutParams) pb_right_top.getLayoutParams();
        lp_small_right_top.rightMargin = top_bottom_small_horizontal;
        lp_small_right_top.topMargin = top_small_top;
        lp_small_right_top.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        pb_right_top.setLayoutParams(lp_small_right_top);
        //右中小圆布局
        RelativeLayout.LayoutParams lp_small_right_middle = (RelativeLayout.LayoutParams) pb_right_middle.getLayoutParams();
        lp_small_right_middle.rightMargin = middle_small_horizontal;
        lp_small_right_middle.topMargin = middle_small_top;
        lp_small_right_middle.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        pb_right_middle.setLayoutParams(lp_small_right_middle);
        //右下小圆布局
        RelativeLayout.LayoutParams lp_small_right_bottom = (RelativeLayout.LayoutParams) pb_right_bottom.getLayoutParams();
        lp_small_right_bottom.rightMargin = top_bottom_small_horizontal;
        lp_small_right_bottom.topMargin = bottom_small_top;
        lp_small_right_bottom.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, RelativeLayout.TRUE);
        pb_right_bottom.setLayoutParams(lp_small_right_bottom);
        // 功能按钮父布局
        RelativeLayout.LayoutParams lp_fun = new RelativeLayout.LayoutParams(w_fun, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lp_fun.topMargin = top_fun;
        lp_fun.leftMargin = fun_left;
        fun_layout.setLayoutParams(lp_fun);
        // 左右功能按钮布局
        RelativeLayout.LayoutParams lp_fun_left = (RelativeLayout.LayoutParams) tv_fun_left.getLayoutParams();
        lp_fun_left.leftMargin = fun_left_left;
        tv_fun_left.setLayoutParams(lp_fun_left);
        RelativeLayout.LayoutParams lp_fun_right = (RelativeLayout.LayoutParams) tv_fun_right.getLayoutParams();
        lp_fun_right.rightMargin = fun_right_right;
        tv_fun_right.setLayoutParams(lp_fun_right);
    }

    private void initData(Context context, AttributeSet attrs) {
        PBCInfo pbcInfoLeftTop = new PBCInfo();
        PBCInfo pbcInfoLeftMiddle = new PBCInfo();
        PBCInfo pbcInfoLeftBottom = new PBCInfo();
        PBCInfo pbcInfoMiddle = new PBCInfo();
        PBCInfo pbcInfoRightTop = new PBCInfo();
        PBCInfo pbcInfoRightMiddle = new PBCInfo();
        PBCInfo pbcInfoRightBottom = new PBCInfo();
        // 设置默认值
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.MultiplePb);
        String titleLeftTop = ta.getString(R.styleable.MultiplePb_title_pbcLeftTop);
        if (!TextUtils.isEmpty(titleLeftTop)) {
            pbcInfoLeftTop.setTitle(titleLeftTop);
        }
        pbcInfoLeftTop.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcLeftTop, ProgressBarController.DEFAULT_VALUE));
        pbcInfoLeftTop.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcLeftTop, ProgressBarController.DEFAULT_VALUE));
        pbcInfoLeftTop.setStyle(1);
        String unitLeftTop = ta.getString(R.styleable.MultiplePb_unit_pbcLeftTop);
        if (!TextUtils.isEmpty(unitLeftTop)) {
            pbcInfoLeftTop.setUnit(unitLeftTop);
        }
        String s_u_LeftTop = ta.getString(R.styleable.MultiplePb_s_u_pbcLeftTop);
        if (!TextUtils.isEmpty(s_u_LeftTop)) {
            pbcInfoLeftTop.setS_u(s_u_LeftTop);
        }
        String titleLeftMiddle = ta.getString(R.styleable.MultiplePb_title_pbcLeftMiddle);
        if (!TextUtils.isEmpty(titleLeftMiddle)) {
            pbcInfoLeftMiddle.setTitle(titleLeftMiddle);
        }
        pbcInfoLeftMiddle.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcLeftMiddle, ProgressBarController.DEFAULT_VALUE));
        pbcInfoLeftMiddle.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcLeftMiddle, ProgressBarController.DEFAULT_VALUE));
        pbcInfoLeftMiddle.setStyle(2);
        String unitLeftMiddle = ta.getString(R.styleable.MultiplePb_unit_pbcLeftMiddle);
        if (!TextUtils.isEmpty(unitLeftMiddle)) {
            pbcInfoLeftMiddle.setUnit(unitLeftMiddle);
        }
        String s_u_LeftMiddle = ta.getString(R.styleable.MultiplePb_s_u_pbcLeftMiddle);
        if (!TextUtils.isEmpty(s_u_LeftMiddle)) {
            pbcInfoLeftMiddle.setS_u(s_u_LeftMiddle);
        }
        String titleLeftBottom = ta.getString(R.styleable.MultiplePb_title_pbcLeftBottom);
        if (!TextUtils.isEmpty(titleLeftBottom)) {
            pbcInfoLeftBottom.setTitle(titleLeftBottom);
        }
        pbcInfoLeftBottom.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcLeftBottom, ProgressBarController.DEFAULT_VALUE));
        pbcInfoLeftBottom.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcLeftBottom, ProgressBarController.DEFAULT_VALUE));
        pbcInfoLeftBottom.setStyle(3);
        String unitLeftBottom = ta.getString(R.styleable.MultiplePb_unit_pbcLeftBottom);
        if (!TextUtils.isEmpty(unitLeftBottom)) {
            pbcInfoLeftBottom.setUnit(unitLeftBottom);
        }
        String s_u_LeftBottom = ta.getString(R.styleable.MultiplePb_s_u_pbcLeftBottom);
        if (!TextUtils.isEmpty(s_u_LeftBottom)) {
            pbcInfoLeftBottom.setS_u(s_u_LeftBottom);
        }
        String titleMiddle = ta.getString(R.styleable.MultiplePb_title_pbcMiddle);
        if (!TextUtils.isEmpty(titleMiddle)) {
            pbcInfoMiddle.setTitle(titleMiddle);
        }
        pbcInfoMiddle.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcMiddle, ProgressBarController.DEFAULT_VALUE));
        pbcInfoMiddle.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcMiddle, ProgressBarController.DEFAULT_VALUE));
        pbcInfoMiddle.setStyle(4);
        String unitMiddle = ta.getString(R.styleable.MultiplePb_unit_pbcMiddle);
        if (!TextUtils.isEmpty(unitMiddle)) {
            pbcInfoMiddle.setUnit(unitMiddle);
        }
        String s_u_Middle = ta.getString(R.styleable.MultiplePb_s_u_pbcMiddle);
        if (!TextUtils.isEmpty(s_u_Middle)) {
            pbcInfoMiddle.setS_u(s_u_Middle);
        }
        String titleRightTop = ta.getString(R.styleable.MultiplePb_title_pbcRightTop);
        if (!TextUtils.isEmpty(titleRightTop)) {
            pbcInfoRightTop.setTitle(titleRightTop);
        }
        pbcInfoRightTop.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcRightTop, ProgressBarController.DEFAULT_VALUE));
        pbcInfoRightTop.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcRightTop, ProgressBarController.DEFAULT_VALUE));
        pbcInfoRightTop.setStyle(5);
        String unitRightTop = ta.getString(R.styleable.MultiplePb_unit_pbcRightTop);
        if (!TextUtils.isEmpty(unitRightTop)) {
            pbcInfoRightTop.setUnit(unitRightTop);
        }
        String s_u_RightTop = ta.getString(R.styleable.MultiplePb_s_u_pbcRightTop);
        if (!TextUtils.isEmpty(s_u_RightTop)) {
            pbcInfoRightTop.setS_u(s_u_RightTop);
        }
        String titleRightMiddle = ta.getString(R.styleable.MultiplePb_title_pbcRightMiddle);
        if (!TextUtils.isEmpty(titleRightMiddle)) {
            pbcInfoRightMiddle.setTitle(titleRightMiddle);
        }
        pbcInfoRightMiddle.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcRightMiddle, ProgressBarController.DEFAULT_VALUE));
        pbcInfoRightMiddle.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcRightMiddle, ProgressBarController.DEFAULT_VALUE));
        pbcInfoRightMiddle.setStyle(6);
        String unitRightMiddle = ta.getString(R.styleable.MultiplePb_unit_pbcRightMiddle);
        if (!TextUtils.isEmpty(unitRightMiddle)) {
            pbcInfoRightMiddle.setUnit(unitRightMiddle);
        }
        String s_u_RightMiddle = ta.getString(R.styleable.MultiplePb_s_u_pbcRightMiddle);
        if (!TextUtils.isEmpty(s_u_RightMiddle)) {
            pbcInfoRightMiddle.setS_u(s_u_RightMiddle);
        }
        String titleRightBottom = ta.getString(R.styleable.MultiplePb_title_pbcRightBottom);
        if (!TextUtils.isEmpty(titleRightBottom)) {
            pbcInfoRightBottom.setTitle(titleRightBottom);
        }
        pbcInfoRightBottom.setOdd(ta.getFloat(R.styleable.MultiplePb_odd_pbcRightBottom, ProgressBarController.DEFAULT_VALUE));
        pbcInfoRightBottom.setTotal(ta.getFloat(R.styleable.MultiplePb_total_pbcRightBottom, ProgressBarController.DEFAULT_VALUE));
        pbcInfoRightBottom.setStyle(7);
        String unitRightBottom = ta.getString(R.styleable.MultiplePb_unit_pbcRightBottom);
        if (!TextUtils.isEmpty(unitRightBottom)) {
            pbcInfoRightBottom.setUnit(unitRightBottom);
        }
        String s_u_RightBottom = ta.getString(R.styleable.MultiplePb_s_u_pbcRightBottom);
        if (!TextUtils.isEmpty(s_u_RightBottom)) {
            pbcInfoRightBottom.setS_u(s_u_RightBottom);
        }

        pb_left_top.setTag(pbcInfoLeftTop);
        pb_left_middle.setTag(pbcInfoLeftMiddle);
        pb_left_bottom.setTag(pbcInfoLeftBottom);
        pb_middle.setTag(pbcInfoMiddle);
        pb_right_top.setTag(pbcInfoRightTop);
        pb_right_middle.setTag(pbcInfoRightMiddle);
        pb_right_bottom.setTag(pbcInfoRightBottom);

        pbcInfoLeftTop = null;
        pbcInfoLeftMiddle = null;
        pbcInfoLeftBottom = null;
        pbcInfoMiddle = null;
        pbcInfoRightTop = null;
        pbcInfoRightMiddle = null;
        pbcInfoRightBottom = null;
    }

    /**
     * 设置左功能健文字内容
     *
     * @param content 文字内容
     */
    public void setLeftFunText(String content) {
        tv_fun_left.setText(content);
    }

    /**
     * 设置右功能健文字内容
     *
     * @param content 文字内容
     */
    public void setRightFunText(String content) {
        tv_fun_right.setText(content);
    }

    /**
     * 重新初始化
     *
     * @param context
     */
//	public void reSet(Context context) {
//		LogEx.d(TAG, "attrs:" + attrs);
//		init(context, attrs);
//		start(true);
//	}

    /**
     * 设置监听器
     */
    private void setListener() {
        pb_middle.setOnClickListener(this);
        decideListener(pb_left_top);
        decideListener(pb_left_middle);
        decideListener(pb_left_bottom);
        decideListener(pb_middle);
        decideListener(pb_right_top);
        decideListener(pb_right_middle);
        decideListener(pb_right_bottom);
    }

    /**
     * 设置上中下进度控件的监听事件
     *
     * @param pbc
     */
    private void decideListener(ProgressBarCircle pbc) {
        if (null != pbc && isSmall(pbc)) {
            pbc.setOnClickListener(pbClickListener);
            pbc.setProgressBarListener(this);
        }
    }

    /**
     * 判断是否是小圆
     *
     * @param pbc
     * @return
     */
    private boolean isSmall(ProgressBarCircle pbc) {
        if (null != pbc && null != pbc.getTag()) {
            if (((PBCInfo) pbc.getTag()).getStyle() != 4) {
                return true;
            }
        }
        return false;
    }

    /**
     * 创建进度控件
     *
     * @param init 是否执行进度动画
     */
    public void start(boolean init) {
        LogEx.d(TAG, "create");
        PBCInfo pbcInfo = null;
        try {
            if (null != pb_left_top && null != pb_left_top.getTag()) {//小圆
                pbcInfo = getClonePBCInfo((PBCInfo) pb_left_top.getTag());
                pb_left_top.createPb(pbcInfo, widthSmall);
            }
            if (null != pb_left_top && null != pb_left_middle.getTag()) {//小圆
                pbcInfo = getClonePBCInfo((PBCInfo) pb_left_middle.getTag());
                pb_left_middle.createPb(pbcInfo, widthSmall);
            }
            if (null != pb_left_bottom && null != pb_left_bottom.getTag()) {//小圆
                pbcInfo = getClonePBCInfo((PBCInfo) pb_left_bottom.getTag());
                pb_left_bottom.createPb(pbcInfo, widthSmall);
            }
            if (null != pb_middle && null != pb_middle.getTag()) {//大圆
                pbcInfo = (PBCInfo) pb_middle.getTag();
                PBCInfo pbcInfoBig = (PBCInfo) pbcInfo.clone();
                pbcInfoBig.setTitle(ProgressBarCircle.getDetailedTitle(pbcInfo.getTitle()));
                widthBig = CircleApplication.getInstance().getWid() * 306 / ProgressBarController.SCREEN_OFFSET;//720宽屏下小圆宽为306dp
                if (pbcInfo.getOdd() < 0) {// 超出总进度
                    pb_middle.createPb(pbcInfoBig, widthBig);
                } else {
                    final float odd = pbcInfo.getOdd();
                    final float total = pbcInfo.getTotal();
                    pbcInfoBig.setOdd(pbcInfo.getTotal());
                    pb_middle.createPb(pbcInfoBig, widthBig);
                    if (init) {
                        postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                pb_middle.startPbAnimation(odd, total, 1000).start();
                            }
                        }, 1000);
                    } else {
                        pb_middle.startFlexAnimationBig(0, true, 1.0f, ProgressBarController.SMALL_CIRCLE_SCALE / ProgressBarController.BIG_CIRCLE_SCALE);
                        lastAnimator();
                    }

                }
                pbcInfoBig = null;
            }
            if (null != pb_right_top && null != pb_right_top.getTag()) {//小圆
                pbcInfo = getClonePBCInfo((PBCInfo) pb_right_top.getTag());
                pb_right_top.createPb(pbcInfo, widthSmall);
            }
            if (null != pb_right_top && null != pb_right_middle.getTag()) {//小圆
                pbcInfo = getClonePBCInfo((PBCInfo) pb_right_middle.getTag());
                pb_right_middle.createPb(pbcInfo, widthSmall);
            }
            if (null != pb_right_bottom && null != pb_right_bottom.getTag()) {//小圆
                pbcInfo = getClonePBCInfo((PBCInfo) pb_right_bottom.getTag());
                pb_right_bottom.createPb(pbcInfo, widthSmall);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pbcInfo = null;
    }

    /**
     * 获取克隆后的PBCInfo
     *
     * @param pbcInfo 小圆原始数据
     * @return
     */
    private PBCInfo getClonePBCInfo(PBCInfo pbcInfo) {
        PBCInfo pbcInfoSmall = null;
        try {
            pbcInfoSmall = (PBCInfo) pbcInfo.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        pbcInfoSmall.setTitle(ProgressBarCircle.getSimpleTitle(pbcInfo.getTitle()));
        return pbcInfoSmall;
    }

    /**
     * 设置内容
     *
     * @param pbcMultiple 动画数据集
     */
    public void setData(PBCMultiple pbcMultiple) {
        LogEx.d(TAG, "setData pbcMultiple:" + pbcMultiple);
        try {
            if (null == pbcMultiple)
                return;
            this.pbcMultiple = pbcMultiple;
            pb_left_top.setTag(pbcMultiple.getL_t_pbcInfo());
            pb_left_middle.setTag(pbcMultiple.getL_m_pbcInfo());
            pb_left_bottom.setTag(pbcMultiple.getL_b_pbcInfo());
            pb_middle.setTag(pbcMultiple.getM_pbcInfo());
            pb_right_top.setTag(pbcMultiple.getR_t_pbcInfo());
            pb_right_middle.setTag(pbcMultiple.getR_m_pbcInfo());
            pb_right_bottom.setTag(pbcMultiple.getR_b_pbcInfo());
            setListener();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置进度控件组的可点击性
     *
     * @param enabled 控件是否可用
     */
    private void setProgressBarEnable(boolean enabled) {
        pb_left_bottom.setEnabled(enabled);
        pb_left_middle.setEnabled(enabled);
        pb_left_top.setEnabled(enabled);
        pb_middle.setEnabled(enabled);
        pb_right_bottom.setEnabled(enabled);
        pb_right_middle.setEnabled(enabled);
        pb_right_top.setEnabled(enabled);
    }

    /**
     * 模拟点击
     */
    public void performClicked() {
        pb_left_top.performClick();
    }

    @Override
    public void onClick(View v) {
        try {
            if (v.getId() == R.id.tv_fun_left) {//左功能按钮点击事件
                if (null != funClickListener) {
                    funClickListener.left_fun_click();
                }
            } else if (v.getId() == R.id.tv_fun_right) {//右功能按钮点击事件
                if (null != funClickListener) {
                    funClickListener.right_fun_click();
                }
            } else if (v.getId() == R.id.pb_middle) {//大圆的点击事件
                if (null != funClickListener) {
                    funClickListener.big_circle_click((PBCInfo) pb_middle.getTag());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 位移后放大动画和进度动画
     */
    private void lastAnimator() {
        AnimatorSet animatorSet = new AnimatorSet();
        Animator animatorFlexNewRight = pb_middle.startFlexAnimationBig(ProgressBarController.FLEX_DURATION_RIGHT, true,
                ProgressBarController.SMALL_CIRCLE_SCALE / ProgressBarController.BIG_CIRCLE_SCALE, 1.0f);
        float odd = ((PBCInfo) pb_middle.getTag()).getOdd();
        float total = ((PBCInfo) pb_middle.getTag()).getTotal();
        Animator animatorPb = pb_middle.startPbAnimation(odd, total, ProgressBarController.PB_DURATION);
        animatorSet.play(animatorPb).after(animatorFlexNewRight);
        animatorSet.start();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        // Log.d("MultiplePb", "onLayout");
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // Log.d("MultiplePb", "onDraw");
    }

    /**
     * 设置控件到左边的距离
     *
     * @param o 1:上控件 2:中控件 3:下控件 4:右控件
     */
    private void setMargin(Object o) {
        int style = 0;
        if (null != o) {
            style = ((PBCInfo) o).getStyle();
            switch (style) {
                case 1:
                    MARGIN_LEFT = getResources().getDimensionPixelSize(R.dimen.dimen_15) - FLEX_OFFSET;
                    break;
                case 2:
                    MARGIN_LEFT = getResources().getDimensionPixelSize(R.dimen.dimen_25) - FLEX_OFFSET / 2;
                    break;
                case 3:
                    MARGIN_LEFT = getResources().getDimensionPixelSize(R.dimen.dimen_15) - FLEX_OFFSET;
                    break;
                default:
                    break;
            }
            MARGIN_TOP = -getResources().getDimensionPixelSize(R.dimen.dimen_22) - FLEX_OFFSET;
        }
    }

    /**
     * 复位
     */
    @Override
    public void recover(ProgressBarCircle pbc) {
        try {
            if (null != pbc.getTag() && null != getBigPBC().getTag()) {
                //交换大小圆标识
                PBCInfo small = (PBCInfo) pbc.getTag();
                int styleSmall = small.getStyle();
                PBCInfo big = (PBCInfo) getBigPBC().getTag();
                int styleBig = big.getStyle();
                small.setStyle(styleBig);
                big.setStyle(styleSmall);
                updateCircleStatic(small);
                updateCircleStatic(big);
                //重新布局
                initView(getContext());
                setProgressBarEnable(false);
                //重导数据
                setData(pbcMultiple);
                //启动动画
                start(false);
                postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setProgressBarEnable(true);// 恢复可点击
                    }
                }, ProgressBarController.FLEX_DURATION_RIGHT + ProgressBarController.PB_DURATION);// lastAnimator();执行完后执行
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 更新大小圆状态
     *
     * @param pbc
     */
    private void updateCircleStatic(PBCInfo pbc) {
        if (null == pbcMultiple)
            return;
        switch (pbc.getStyle()) {
            case 1://左上小圆
                pbcMultiple.setL_t_pbcInfo(pbc);
                break;
            case 2://左中小圆
                pbcMultiple.setL_m_pbcInfo(pbc);
                break;
            case 3://左下小圆
                pbcMultiple.setL_b_pbcInfo(pbc);
                break;
            case 4://中间大圆
                pbcMultiple.setM_pbcInfo(pbc);
                break;
            case 5://右上小圆
                pbcMultiple.setR_t_pbcInfo(pbc);
                break;
            case 6://右中小圆
                pbcMultiple.setR_m_pbcInfo(pbc);
                break;
            case 7://右下小圆
                pbcMultiple.setR_b_pbcInfo(pbc);
                break;
            default:
                break;
        }
    }

    /**
     * 改变上中下右的标志
     *
     * @param pbc
     *            要改变的小控件
     */
//	private void changeStyle(ProgressBarCircle pbc) {
//		if (null != pbc.getTag()) {
//			PBCInfo pbcInfo = (PBCInfo) pbc.getTag();
//			int style = pbcInfo.getStyle();
//			pbcInfo.setStyle(4);
//			pbc.setTag(pbcInfo);
//			ProgressBarCircle pbcBig = getBigPBC();
//			if (null != pbcBig.getTag()) {
//				pbcInfo = (PBCInfo) pbcBig.getTag();
//				pbcInfo.setStyle(style);
//				pbcBig.setTag(pbcInfo);
//			}
//			pbcInfo = null;
//		}
//	}

    /**
     * 获取大控件
     *
     * @return
     */
    private ProgressBarCircle getBigPBC() {
        if (isBigPBC(pb_middle)) {
            return pb_middle;
        } else if (isBigPBC(pb_left_bottom)) {
            return pb_left_bottom;
        } else if (isBigPBC(pb_left_middle)) {
            return pb_left_middle;
        } else if (isBigPBC(pb_left_top)) {
            return pb_left_top;
        }
        return pb_middle;
    }

    /**
     * 是否是大控件
     *
     * @param pbc
     * @return
     */
    private boolean isBigPBC(ProgressBarCircle pbc) {
        if (null != pbc.getTag()) {
            if (((PBCInfo) pbc.getTag()).getStyle() == 4) {
                return true;
            }
        }
        return false;
    }

    /**
     * 获取点击动画所需数据
     *
     * @param PBC     小圆
     * @param pbcInfo 小圆数据
     * @return
     */
    private PBCClickInfo getPBCClickInfo(ProgressBarCircle PBC, PBCInfo pbcInfo) {
        if (null == pbcInfo) {
            return null;
        }
        PBCClickInfo pbcClickInfo = new PBCClickInfo();
        if (isSmall(PBC)) {
            if (null == PBC.getTag()) {
                pbcInfo = null;
                return pbcClickInfo;
            }
            pbcClickInfo.setMaxSmall(pbcInfo.getTotal());
            pbcClickInfo.setCurrSmall(pbcInfo.getOdd());
            pbcClickInfo.setPbcSmall(PBC);
            pbcClickInfo.setWidthSmall(PBC.getW_h());
            pbcClickInfo.setAnimatorFlexSmall(PBC.startFlexAnimationSmall(pbcInfo.getTotal(), pbcInfo.getOdd(), ProgressBarController.FLEX_DURATION, 1.0f, 0.8f, 1.0f));
        }
        pbcInfo = null;
        return pbcClickInfo;
    }

    /**
     * 获取点击动画所需数据
     *
     * @param id 控件id
     * @return
     */
    private PBCClickInfo getPBCClickInfo(int id) {
        switch (id) {// 小圆缩小动画
            case R.id.pb_left_top:// 原左上 默认省内进度
                return getPBCClickInfo(pb_left_top, (PBCInfo) pb_left_top.getTag());
            case R.id.pb_left_middle:// 原左中 默认本地进度
                return getPBCClickInfo(pb_left_middle, (PBCInfo) pb_left_middle.getTag());
            case R.id.pb_left_bottom:// 原左下 默认定向进度
                return getPBCClickInfo(pb_left_bottom, (PBCInfo) pb_left_bottom.getTag());
            case R.id.pb_middle:// 原中间 默认国内
                return getPBCClickInfo(pb_middle, (PBCInfo) pb_middle.getTag());
            case R.id.pb_right_top:// 原左上 默认省内进度
                return getPBCClickInfo(pb_right_top, (PBCInfo) pb_right_top.getTag());
            case R.id.pb_right_middle:// 原左中 默认本地进度
                return getPBCClickInfo(pb_right_middle, (PBCInfo) pb_right_middle.getTag());
            case R.id.pb_right_bottom:// 原左下 默认定向进度
                return getPBCClickInfo(pb_right_bottom, (PBCInfo) pb_right_bottom.getTag());
            default:
                break;
        }
        return new PBCClickInfo();
    }

    /**
     * 设置功能按钮点击监听器
     *
     * @param funClickListener
     */
    public void setFunClickListener(FunClickListener funClickListener) {
        this.funClickListener = funClickListener;
    }

    class PbClickListener implements OnClickListener {
        @Override
        public void onClick(View v) {
            try {
                if (null == pbcMultiple) {
                    Toast.makeText(CircleApplication.getInstance(), "数据为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                float endSmallThree = 0;
                float maxBig = 0;//大圆进度最大值
                float currBig = 0;//大圆当前进度值
                int widthBig = 0;//大圆宽度值
                float maxSmall = 0;//小圆进度最大值
                float currSmall = 0;//小圆当前进度
                int widthSmall = 0;//小圆宽度值
                Animator animatorFlexSmall = null;//小圆收缩动画
                ProgressBarCircle pbcSmall = null;//小圆
                ProgressBarCircle pbcBig = null;
                PBCClickInfo pbcClickInfo = null;
                AnimatorSet animatorSet = new AnimatorSet();
                setProgressBarEnable(false);
                pbcClickInfo = getPBCClickInfo(v.getId());
                if (null == pbcClickInfo) {
                    setProgressBarEnable(true);
                    Toast.makeText(CircleApplication.getInstance(), "数据为空", Toast.LENGTH_SHORT).show();
                    return;
                }
                animatorFlexSmall = pbcClickInfo.getAnimatorFlexLeft();
                pbcSmall = pbcClickInfo.getPbcSmall();
                maxSmall = pbcClickInfo.getMaxSmall();
                currSmall = pbcClickInfo.getCurrSmall();
                widthSmall = pbcClickInfo.getWidthSmall();
                pbcClickInfo = null;
                pbcBig = getBigPBC();
                endSmallThree = maxSmall;
                maxBig = ((PBCInfo) pbcBig.getTag()).getTotal();
                widthBig = pbcBig.getMeasuredWidth();
                currBig = ((PBCInfo) pbcBig.getTag()).getOdd();
                int big_plus = widthBig - widthSmall;// 大圆缩小前后的差 用于移动动画
                int x_small = pbcSmall.getLeft();
                int y_small = pbcSmall.getTop();
                int x_big = pbcBig.getLeft();
                int y_big = pbcBig.getTop();
                int backgroundColorBig = 0;
                int fontColorBig = 0;
                float startBigOne = 0;
                float txtBigOne = 0;
                if (pbcBig.getProgressIsOut()) {// 超出总进度
                    backgroundColorBig = getResources().getColor(ProgressBarController.OUT_INNER_BACKGROUND_COLOR_SMALL);
                    fontColorBig = getResources().getColor(ProgressBarController.OUT_FONT_COLOR_BIG);
                    startBigOne = ProgressBarController.SWEEP_ARC_ANGLE;// 满弧
                    txtBigOne = currBig;
                } else {
                    backgroundColorBig = getResources().getColor(ProgressBarController.DEFAULT_INNER_BACKGROUND_COLOR_SMALL);
                    fontColorBig = getResources().getColor(ProgressBarController.DEFAULT_FONT_COLOR_SMALL);
                    startBigOne = ((PBCInfo) pbcBig.getTag()).getOdd();
                    txtBigOne = maxBig;
                }
                int backgroundColorSmall = 0;
                float txtSmallThree = 0;// 当前值为最大值
                if (pbcSmall.getProgressIsOut()) {// 超出总进度
                    endSmallThree = ProgressBarController.SWEEP_ARC_ANGLE;// 从满弧到0
                    backgroundColorSmall = getResources().getColor(ProgressBarController.OUT_INNER_BACKGROUND_COLOR_BIG);
                    txtSmallThree = currSmall;
                } else {
                    backgroundColorSmall = getResources().getColor(ProgressBarController.DEFAULT_INNER_BACKGROUND_COLOR_BIG);
                    txtSmallThree = maxSmall;
                }

                // 大圆动画
                Animator animatorFlexBig = pbcBig.startFlexAnimationBig(ProgressBarController.FLEX_DURATION_RIGHT, false, 1.0f,
                        ProgressBarController.SMALL_CIRCLE_SCALE / ProgressBarController.BIG_CIRCLE_SCALE);// 大圆缩小动画(标题详缩写设置)
                Animator animatorPbBigBackgroundColor = pbcBig
                        .startColorAnimation(0, ProgressBarController.PB_DURATION + ProgressBarController.PB_DURATION, true,
                                pbcBig.getInnerPbBackGroundColor(), backgroundColorBig);//大圆文字和内部圆背景变色
                Animator animatorPbBigFontColor = pbcBig.startColorAnimation(1, ProgressBarController.PB_DURATION + ProgressBarController.PB_DURATION,
                        true, getResources().getColor(ProgressBarController.DEFAULT_FONT_COLOR_BIG), fontColorBig);//大圆外部进度条变色（允许ProgressBarCircle的圆大小状态改变）
                Animator animatorPbBigOne = pbcBig.startCurrProgressBarAnimation(false, ProgressBarController.PB_DURATION, txtBigOne, 0, startBigOne, 0);//大圆当前进度条有到无动画
                Animator animatorPbBigTwo = pbcBig.startDefaultProgressBarAnimation(true, ProgressBarController.PB_DURATION,
                        ProgressBarController.SWEEP_ARC_ANGLE, 0);//大圆默认进度条有到无动画(ProgressBarCircle的圆大小状态设为小圆)
                Animator animatorPbBigThree = pbcBig.startCircleProgressBarAnimation(ProgressBarController.PB_DURATION, 0, currBig, 0,
                        ProgressBarController.SWEEP_CIRCLE_ANGLE);//大圆进度条无到满弧动画
                Animator animatorTranslateBig = pbcBig.startTranslateAnimation(x_small - big_plus / 2 - x_big, y_small - big_plus / 2 - y_big);//大圆位移动画
                // 小圆动画
                Animator animatorPbSmallBackgroundColor = pbcSmall.startColorAnimation(0, ProgressBarController.PB_DURATION
                        + ProgressBarController.PB_DURATION, false, pbcSmall.getInnerPbBackGroundColor(), backgroundColorSmall);//小圆文字和内部圆背景变色（改变ProgressBarCircle的圆大小状态为大圆）
                Animator animatorPbSmallFontColor = pbcSmall.startColorAnimation(1, ProgressBarController.PB_DURATION + ProgressBarController.PB_DURATION,
                        false, pbcSmall.getInnerFontColor(), getResources().getColor(ProgressBarController.DEFAULT_FONT_COLOR_BIG));//小圆外部进度条变色
                Animator animatorPbSmallOne = pbcSmall.startCircleProgressBarAnimation(ProgressBarController.PB_DURATION, currSmall, 0,
                        ProgressBarController.SWEEP_CIRCLE_ANGLE, 0);//小圆从满圆到无动画
                Animator animatorPbSmallTwo = pbcSmall.startDefaultProgressBarAnimation(false, ProgressBarController.PB_DURATION, 0,
                        ProgressBarController.SWEEP_ARC_ANGLE);//小圆默认进度条从无到满弧动画
                Animator animatorPbSmallThree = pbcSmall.startCurrProgressBarAnimation(true, ProgressBarController.PB_DURATION, 0, txtSmallThree, 0,
                        endSmallThree);//小圆当前进度条从无到有动画
                Animator animatorTranslateSmall = pbcSmall.startTranslateAnimation(x_big + big_plus / 2 - x_small, y_big + big_plus / 2 - y_small);//小圆位移动画
                setMargin(pbcSmall.getTag());

                animatorSet.play(animatorFlexSmall).with(animatorFlexBig);
                animatorSet.play(animatorPbBigBackgroundColor).with(animatorPbBigFontColor).with(animatorPbBigOne).with(animatorPbBigTwo)
                        .with(animatorPbSmallOne).after(animatorFlexBig);
                animatorSet.play(animatorPbBigThree).after(animatorPbBigTwo);
                animatorSet.play(animatorPbSmallBackgroundColor).with(animatorPbSmallFontColor).with(animatorPbSmallTwo).with(animatorPbSmallThree)
                        .after(animatorPbSmallOne);
                animatorSet.play(animatorTranslateBig).with(animatorTranslateSmall).after(animatorPbSmallThree);
                animatorSet.start();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
