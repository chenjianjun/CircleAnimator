package com.example.circleanimation.listener;


import com.example.circleanimation.widget.ProgressBarCircle;

/**
 * 进度监听器
 * Author：cjj
 * Blog：jianjunchen.net
 */
public interface ProgressBarListener {
    /**
     * 状态恢复
     */
    void recover(ProgressBarCircle pbc);
}
