package com.example.circleanimation.util;

import com.example.circleanimation.R;

/**
 * 属性控制器
 * Author：cjj
 * Blog：jianjunchen.net
 * 全局变量
 */
public class ProgressBarController {
    //	public static final int DEFAULT_START = 0;
    public static final float DEFAULT_VALUE = -1.111111f;//默认值，若为默认值则显示时为“- -”
    public static final int FLEX_DURATION = 500;// 伸缩动画持续时间
    public static final int FLEX_DURATION_RIGHT = 800;// 伸缩动画持续时间
    public static final int PB_DURATION = 500;// 动画持续时间
//	public static final int TRANSLATE_DURATION = 500;// 位移动画持续时间

    public static final int SCREEN_OFFSET = 720;//参考屏幕分辨率宽度
    public static final int SWEEP_ARC_ANGLE = 270;// 圆弧划过的角度(圆弧最大角度)
    public static final int SWEEP_ARC_ANGLE_START = 135;//默认圆弧的开始角度
    public static final int SWEEP_CIRCLE_ANGLE = -360;// 逆时针旋转的角度
    public static final float BIG_CIRCLE_SCALE = 306.0f;//720屏宽下的大圆宽高
    public static final float SMALL_CIRCLE_SCALE = 113.0f;//720屏宽下的小圆宽高
    public static final int BIG_CIRCLE_STROKE = 14;//大圆外边距
    public static final int BIG_CIRCLE_INTERVAL = 36;//大圆圆弧和内圆的间隔
    public static final int BIG_CIRCLE_INNER_CIRCLE_SCALE = 266;//大圆内部圆直径

    public static final int TITLE_TS_SMALL = 5;//小圆的宽度与小圆标题的文字大小比
    public static final int TITLE_TS_BIG = 10;//大圆的宽度与大圆标题的文字大小比
    public static final int TEXT_U_TS_SMALL = 8;//小圆的宽度与小圆内容单位的文字大小比
    public static final int TEXT_U_TS_BIG = 16;//大圆的宽度与大圆内容单位的文字大小比
    public static final int TEXT_TS = 4;//圆的宽度与圆内容的文字大小比
    public static final int PERCENT_TS = 10;//圆的宽度与圆百分比的文字大小比

    public static final int DEFAULT_FONT_COLOR_BIG = R.color._ffffff;//大圆内部圆文字颜色
    public static final int DEFAULT_INNER_BACKGROUND_COLOR_BIG = R.color._fe9602;//大圆内部圆背景默认颜色
    public static final int OUT_INNER_BACKGROUND_COLOR_BIG = R.color._e73330;//大圆超出总进度的内部圆背景颜色
    public static final int CURR_EXTERNAL_COLOR_BIG = R.color._ff9025;//大圆当前进度条颜色
    public static final int DEFAULT_EXTERNAL_COLOR_BIG = R.color._d8d8d8;//大圆默认进度条颜色
    public static final int OUT_EXTERNAL_COLOR_BIG = R.color._e83431;//大圆超出总进度的进度条颜色
    public static final int DEFAULT_INNER_BACKGROUND_COLOR_SMALL = R.color._fec36f;//小圆内部圆背景默认颜色
    public static final int DEFAULT_FONT_COLOR_SMALL = R.color._ff9025;//小圆内部圆文字颜色
    public static final int CURR_EXTERNAL_COLOR_SMALL = R.color._fec36f;//小圆默认进度条颜色
    public static final int OUT_INNER_BACKGROUND_COLOR_SMALL = R.color._e56866;//小圆超出总进度的内部圆背景颜色
    public static final int OUT_FONT_COLOR_BIG = R.color._e73330;//超出总进度小圆内部圆文字颜色
    public static final int OUT_EXTERNAL_COLOR_SMALL = R.color._fd4456;//小圆超出总进度的进度条颜色

    public static final int TITLE_FONT_COUNT = 2;//标题简写保留前几位

    /**动画顺序*/
//	public static final String CHECK = "check";
}
