package com.example.circleanimation.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import com.example.circleanimation.R;
import com.example.circleanimation.util.ProgressBarController;


/**
 * 外部进度条
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class ExternalProgressBar extends View {

    // 默认颜色， 当前进度颜色
    private int defaultPbColor, currPbColor;
    // 是否已超出
    private boolean progressIsOut;
    //进度条宽度, 弧形半径，当前进度, 跟内部圆的间隔
    private float strokeWidth, pbRadius, currPb, interval;
    private Paint paint;
    //内部圆左坐标，内部圆上坐标(注意：值要一样)
    private float innerLeft, innerTop;//跟InnerRoundProgress的marginLeft、marginRight相同
    //小圆圆心到弧形圆心的距离, 圆弧内侧左边距(不包含stroke), 圆弧内侧上边距(不包含stroke), 小圆半径
    private float smallCc_r, margin_left, magin_top, radius_small;
    //偏移值（半径+左边距）, 偏移值（半径+上边距）
    private float x_offset, y_offset;
    /**
     * 是否缩小了
     */
    private boolean pbIsSmall = false;
    private float defaultSweep = 0;//默认圆弧的划过角度
    private boolean showLeftCircle = true;//是否显示左边默认圆
    private boolean showRightCircle = true;//是否显示右边默认圆
    private boolean showRightCircleCur = true;//是否显示右边进度圆
    private boolean showLeftCircleCur = true;//是否显示左边进度圆
    private float startAngle = ProgressBarController.SWEEP_ARC_ANGLE_START;//默认圆弧的开始角度
    private float centerCircle = 0;//圆心


    public ExternalProgressBar(Context context) {
        super(context, null, 0);
    }

    public ExternalProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ExternalProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ExternalProgressBar);
        defaultPbColor = ta.getInteger(R.styleable.ExternalProgressBar_defaultPbColor, 0);
        currPbColor = ta.getInteger(R.styleable.ExternalProgressBar_currPbColor, 0);
        progressIsOut = ta.getBoolean(R.styleable.ExternalProgressBar_progressIsOut, false);
        strokeWidth = ta.getDimension(R.styleable.ExternalProgressBar_strokeWidth, 0);
        pbRadius = ta.getDimension(R.styleable.ExternalProgressBar_pbRadius, 0);
        innerLeft = ta.getDimension(R.styleable.ExternalProgressBar_innerLeft, 0);
        innerTop = ta.getDimension(R.styleable.ExternalProgressBar_innerTop, 0);
        interval = ta.getDimension(R.styleable.ExternalProgressBar_interval, 0);
        currPb = ta.getFloat(R.styleable.ExternalProgressBar_currPb, 0);
        ta.recycle();
    }

    @SuppressLint("NewApi")
    public ExternalProgressBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        this(context, attrs, defStyleAttr);
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        try {
            margin_left = innerLeft - interval;//圆弧内侧左边距(不包含stroke)
            magin_top = innerTop - interval;//圆弧内侧上边距(不包含stroke)
            RectF reF = new RectF(margin_left + strokeWidth, magin_top + strokeWidth, margin_left + pbRadius * 2 + strokeWidth, magin_top + pbRadius * 2 + strokeWidth);//包含一半stroke(要考虑边距)
            paint = new Paint();
            paint.setStyle(Style.STROKE);//半径内包含一半stroke
            paint.setAntiAlias(true);
            paint.setStrokeWidth(strokeWidth);

            if (!pbIsSmall) {//大圆弧
                smallCc_r = pbRadius;//小圆圆心到弧形圆心的距离(弧形半径包含strokeWidth/2)
                radius_small = strokeWidth / 2;//小圆半径
                x_offset = pbRadius + margin_left + strokeWidth;//偏移值（半径+左边距）
                y_offset = pbRadius + magin_top + strokeWidth;//偏移值（半径+上边距）
                float x_y_small = (float) Math.sqrt(Math.pow(smallCc_r, 2) / 2);//小圆圆心到X轴/y轴的距离
                //画默认圆弧左边起始圆
                float x_left = x_offset - x_y_small;//起始圆的x坐标
                float y_left_right = y_offset + x_y_small;//起始圆/终点圆的y坐标
                //画默认圆弧右边终点圆
                float x_right = x_offset + x_y_small;//终点圆的x坐标
                if (!progressIsOut) {
                    drawNormal(canvas, reF, x_left, y_left_right, x_right);
                } else {
                    drawOut(canvas, reF, x_left, y_left_right, x_right);
                }
            } else {//小圆
                paint.setColor(currPbColor);
                canvas.drawArc(reF, startAngle, currPb, false, paint);//弧形所在矩形区域、起始角度、圆弧扫过的角度(顺时针方向，单位为度,从右中间开始为零度)、是否包括圆心（是：扇形圆弧；否：外层圆弧）、画笔
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 超出总进度的绘制
     *
     * @param canvas       画布
     * @param reF          圆弧所在矩形区域
     * @param x_left       左边小圆x坐标
     * @param y_left_right 左右边小圆Y坐标
     * @param x_right      右边小圆x坐标
     */
    private void drawOut(Canvas canvas, RectF reF, float x_left, float y_left_right, float x_right) {
        paint.setColor(defaultPbColor);
        //画超出总进度的圆弧
        drawDefault(canvas, reF, x_left, y_left_right, x_right);
    }

    /**
     * 画默认圆弧（包括小圆）
     *
     * @param canvas       画布
     * @param reF          圆弧所在矩形区域
     * @param x_left       左边小圆x坐标
     * @param y_left_right 左右边小圆Y坐标
     * @param x_right      右边小圆x坐标
     */
    private void drawDefault(Canvas canvas, RectF reF, float x_left, float y_left_right, float x_right) {
        //画默认圆弧
        if (progressIsOut) {//超出总进度
            defaultSweep = currPb;//270  为实现动画
        }
        canvas.drawArc(reF, 135, defaultSweep, false, paint);//弧形所在矩形区域、起始角度、圆弧扫过的角度(顺时针方向，单位为度,从右中间开始为零度)、是否包括圆心（是：扇形圆弧；否：外层圆弧）、画笔

        paint.setStyle(Style.FILL);//半径内不包含此stroke
        paint.setStrokeWidth(0);
        //画默认圆弧左边起始圆
        if (showLeftCircle) {
            canvas.drawCircle(x_left, y_left_right, radius_small, paint);
        }
        //画默认圆弧右边终点圆
        if (showRightCircle) {
            canvas.drawCircle(x_right, y_left_right, radius_small, paint);
        }
    }

    /**
     * 正常情况下的绘制
     * 没有超出总进度
     *
     * @param canvas       画布
     * @param reF          圆弧所在矩形区域
     * @param x_left       左边小圆x坐标
     * @param y_left_right 左右边小圆Y坐标
     * @param x_right      右边小圆x坐标
     */
    private void drawNormal(Canvas canvas, RectF reF, float x_left, float y_left_right, float x_right) {

        paint.setColor(defaultPbColor);

        drawDefault(canvas, reF, x_left, y_left_right, x_right);

        paint.setColor(currPbColor);

        //画进度圆弧左边起始圆
        if (showLeftCircleCur) {
            canvas.drawCircle(x_left, y_left_right, radius_small, paint);
        }

        //画进度
        drawProgress(canvas, reF);
    }

    /**
     * 画进度
     *
     * @param canvas 画布
     * @param reF    圆弧所在矩形区域
     */
    private void drawProgress(Canvas canvas, RectF reF) {
        //画圆弧进度（右水平线为起点，顺时针为正方向）
        if (showRightCircleCur) {
            float angle = 225 - currPb;//以左x轴为起点划过的度数
            //画进度右边终点圆
            float x_cur = 0;
            float y_cur = 0;
            if (angle > 90 && angle <= 270) {//第二、三区域（y轴左边 ）
                x_cur = (float) (x_offset - smallCc_r * Math.cos(Math.abs(180 - angle) * Math.PI / 180));//cos（数值）——数值=角度/(180/Math.PI)
            } else if (angle < 90 && angle > 0) {//第一区域（y轴右上边 ）
                x_cur = (float) (x_offset + smallCc_r * Math.cos(angle * Math.PI / 180));
            } else if (angle == 0) {
                x_cur = x_offset + pbRadius + strokeWidth / 2;
            } else if (angle == 90) {
                x_cur = 0;
            } else {//第四区域（y轴右下边 ）
                x_cur = (float) (x_offset + smallCc_r * Math.cos(Math.abs(angle) * Math.PI / 180));
            }
            if (angle > 0 && angle < 180) {//第一、二区域（x轴上边）
                y_cur = y_offset - (float) (smallCc_r * Math.sin(Math.abs(180 - angle) * Math.PI / 180));
            } else if (angle == 0 || angle == 180) {
                y_cur = y_offset;
            } else if (angle > 180 && angle <= 270) {//第三区域（x轴左下边）
                y_cur = y_offset + (float) (smallCc_r * Math.sin((angle - 180) * Math.PI / 180));
            } else if (angle >= -90 && angle < 0) {//第四区域（x轴右下边）
                y_cur = y_offset + (float) (smallCc_r * Math.sin(Math.abs(angle) * Math.PI / 180));
            }
            canvas.drawCircle(x_cur, y_cur, radius_small, paint);
        }
        paint.setStyle(Style.STROKE);//半径内不包含此stroke
        paint.setStrokeWidth(strokeWidth);
        canvas.drawArc(reF, 135, currPb, false, paint);//弧形所在矩形区域、起始角度、终止角度、是否包括圆心（是：扇形圆弧；否：外层圆弧）、画笔

    }

    /**
     * 设置当前进度
     *
     * @param currPb
     */
    public void setCurrPb(float currPb) {
        this.currPb = currPb;
    }

    /**
     * 设置默认进度条颜色
     *
     * @param defaultPbColor
     */
    public void setDefaultPbColor(int defaultPbColor) {
        this.defaultPbColor = defaultPbColor;
    }

    /**
     * 设置当前进度条颜色
     *
     * @param currPbColor
     */
    public void setCurrPbColor(int currPbColor) {
        this.currPbColor = currPbColor;
    }

    /**
     * 设置总进度是否已超出
     *
     * @param progressIsOut
     */
    public void setProgressIsOut(boolean progressIsOut) {
        this.progressIsOut = progressIsOut;
    }

    /**
     * 设置进度条宽度
     *
     * @param strokeWidth
     */
    public void setStrokeWidth(float strokeWidth) {
        this.strokeWidth = strokeWidth;
    }

    /**
     * 设置弧形半径
     *
     * @param pbRadius
     */
    public void setPbRadius(float pbRadius) {
        this.pbRadius = pbRadius;
    }

    /**
     * 设置内部圆左边距
     */
    public void setInnerLeft(float innerLeft) {
        this.innerLeft = innerLeft;
    }

    /**
     * 设置内部圆上边距
     */
    public void setInnerTop(float innerTop) {
        this.innerTop = innerTop;
    }

    /**
     * 设置跟内部圆的间隔
     *
     * @param interval
     */
    public void setInterval(float interval) {
        this.interval = interval;
    }

    /**
     * 获取圆大小 状态
     */
    public boolean getPbIsSmall() {
        return pbIsSmall;
    }

    /**
     * 设置圆大小 状态
     *
     * @param pbIsSmall
     */
    public void setPbIsSmall(boolean pbIsSmall) {
        this.pbIsSmall = pbIsSmall;
    }

    /**
     * 设置默认圆弧划过的角度
     *
     * @param defaultSweep
     */
    public void setDefaultSweep(float defaultSweep) {
        this.defaultSweep = defaultSweep;
    }

    /**
     * 设置是否显示左边小圆
     *
     * @param showLeftCircle
     */
    public void setShowLeftCircle(boolean showLeftCircle) {
        this.showLeftCircle = showLeftCircle;
    }

    /**
     * 设置是否显示左边小圆
     *
     * @param showLeftCircleCur
     */
    public void setShowLeftCircleCur(boolean showLeftCircleCur) {
        this.showLeftCircleCur = showLeftCircleCur;
    }

    /**
     * 设置是否显示默认右边小圆
     *
     * @param showRightCircle
     */
    public void setShowRightCircle(boolean showRightCircle) {
        this.showRightCircle = showRightCircle;
    }

    /**
     * 设置是否显示默认右边小圆
     *
     * @param showRightCircleCur
     */
    public void setShowRightCircleCur(boolean showRightCircleCur) {
        this.showRightCircleCur = showRightCircleCur;
    }

    /**
     * 设置默认圆弧的开始角度
     * @param startAngle
     */
//	public void setStartAngle(float startAngle) {
//		this.startAngle = startAngle;
//	}

    /**
     * 设置圆心坐标
     * @param centerCircle
     */
//	public void setCenterCircle(float centerCircle) {
//		this.centerCircle = centerCircle;
//	}

}
