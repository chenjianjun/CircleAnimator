package com.example.circleanimation.widget;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.widget.RelativeLayout;

import com.example.circleanimation.R;
import com.example.circleanimation.listener.ProgressBarListener;
import com.example.circleanimation.model.PBCInfo;
import com.example.circleanimation.util.LogEx;
import com.example.circleanimation.util.ProgressBarController;
import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.ArgbEvaluator;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.PropertyValuesHolder;
import com.nineoldandroids.animation.ValueAnimator;
import com.nineoldandroids.animation.ValueAnimator.AnimatorUpdateListener;
import com.nineoldandroids.view.ViewHelper;

import java.text.DecimalFormat;

/**
 * 自定义进度控件
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class ProgressBarCircle extends RelativeLayout {
    private static final String TAG = "ProgressBarCircle";

    private ExternalProgressBar externalPb;// 外部进度条
    private InnerRoundProgressBar innerPbBar;// 内部圆
    private boolean pbIsSmall;// 是否是小圆
    private float max;// 最大值
    private boolean progressIsOut = false;// 是否超最大值
    private int w_h;// 控件宽度
    private boolean change = true;// 是否在控件变化大小后改变圆大小的状态
    private String title;// 标题
    private float curr;// 当前进度
    private ProgressBarListener listener;
    private float strokeWidth;//边距
    private boolean isFloat = false;//动画是否支持浮点,默认不支持
    private String unit = "";//内容单位
    private String s_u = "";//内容单位缩写

    public ProgressBarCircle(Context context) {
        this(context, null);
    }

    public ProgressBarCircle(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ProgressBarCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * 使用java正则表达式去掉多余的.与0
     *
     * @param s 字符串入餐
     * @return
     */
    public static String subZeroAndDot(String s) {
        if (s.indexOf(".") > 0) {
            s = s.replaceAll("0+?$", "");// 去掉多余的0
            s = s.replaceAll("[.]$", "");// 如最后一位是.则去掉
        }
        return s;
    }

    /**
     * 获取标题详写
     *
     * @param s
     * @return
     */
    public static String getDetailedTitle(String s) {
        if (null != s && s.length() <= ProgressBarController.TITLE_FONT_COUNT) {
            s += "剩余量";
        }
        return s;
    }

    /**
     * 获取边距
     *
     * @return
     */
//    public float getStrokeWidth() {
//        return strokeWidth;
//    }

    /**
     * 获取标题简写
     *
     * @param s
     * @return
     */
    public static String getSimpleTitle(String s) {
        if (null != s && s.length() > ProgressBarController.TITLE_FONT_COUNT) {
            s = s.substring(0, ProgressBarController.TITLE_FONT_COUNT);
        }
        return s;
    }

    private void init(Context context, AttributeSet attrs) {
        LogEx.d(TAG, "init：" + context);
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.ProgressBarCircle);
        pbIsSmall = ta.getBoolean(R.styleable.ProgressBarCircle_isSmall, true);
        ta.recycle();

        // View mainView =
        // LayoutInflater.from(context).inflate(R.layout.progressbar_circle,
        // null);
        // addView(mainView);

        externalPb = new ExternalProgressBar(context);
        innerPbBar = new InnerRoundProgressBar(context);
        addView(externalPb);
        addView(innerPbBar);
        // 初始化进度控件
        // ViewTreeObserver vto2 = getViewTreeObserver();
        // vto2.addOnGlobalLayoutListener(new OnGlobalLayoutListener() {
        // @Override
        // public void onGlobalLayout() {
        // getViewTreeObserver().removeGlobalOnLayoutListener(this);
        // w_h = getMeasuredHeight();// 166dp
        // reInit();
        // }
        // });

    }

    /**
     * 创建进度控件
     *
     * @param pbcInfo 控件数据
     * @param wid     控件宽度
     */
    public void createPb(PBCInfo pbcInfo, int wid) {
        w_h = wid;
        setIsFloat(pbcInfo.getIsFloat());
        unit = pbcInfo.getUnit();
        s_u = pbcInfo.getS_u();
        title = pbcInfo.getTitle();
        curr = pbcInfo.getOdd();
        max = pbcInfo.getTotal();
        if (curr < 0 && curr != ProgressBarController.DEFAULT_VALUE) {// 超总进度了
            progressIsOut = true;
        }
        externalPb.setPbIsSmall(pbIsSmall);
//        innerPbBar.setPbIsSmall(pbIsSmall);
        innerPbBar.setTxtExpand(pbIsSmall);
        innerPbBar.setTitle(title);
        String odd = " - -";
        String pb_inner = " - -";
        float pb_external = 0;
        externalPb.setShowLeftCircleCur(false);
        externalPb.setShowRightCircleCur(false);
        if (curr != ProgressBarController.DEFAULT_VALUE && max != ProgressBarController.DEFAULT_VALUE) {
            odd = getConsumeCircle(curr);
            pb_inner = getProgressCircleBaiFenBi(curr, max);
            pb_external = getProgressArcPer(curr, max);
            if (0 != curr && 0 != max) {
                externalPb.setShowLeftCircleCur(true);
                externalPb.setShowRightCircleCur(true);
            }
        }
        innerPbBar.setText(odd);
        if (!pbIsSmall) {// 大圆
            innerPbBar.setText_u(pbcInfo.getUnit());
            innerPbBar.setTextColor(getResources().getColor(ProgressBarController.DEFAULT_FONT_COLOR_BIG));
            externalPb.setProgressIsOut(progressIsOut);
            if (!progressIsOut) {// 正常
                innerPbBar.setProgressIsDisplay(true);
                innerPbBar.setBackgroundColor(getResources().getColor(ProgressBarController.DEFAULT_INNER_BACKGROUND_COLOR_BIG));
                innerPbBar.setProgress(pb_inner);
                externalPb.setCurrPb(pb_external);
                externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.CURR_EXTERNAL_COLOR_BIG));
                externalPb.setDefaultPbColor(getResources().getColor(ProgressBarController.DEFAULT_EXTERNAL_COLOR_BIG));
                externalPb.setDefaultSweep(ProgressBarController.SWEEP_ARC_ANGLE);
            } else {// 超出总进度
                innerPbBar.setProgressIsDisplay(false);
                innerPbBar.setBackgroundColor(getResources().getColor(ProgressBarController.OUT_INNER_BACKGROUND_COLOR_BIG));
                externalPb.setDefaultPbColor(getResources().getColor(ProgressBarController.OUT_EXTERNAL_COLOR_BIG));
                externalPb.setCurrPb(ProgressBarController.SWEEP_ARC_ANGLE);// 满弧
            }
        } else {// 小圆
            innerPbBar.setText_u(pbcInfo.getS_u());
            externalPb.setCurrPb(ProgressBarController.SWEEP_CIRCLE_ANGLE);
            innerPbBar.setProgressIsDisplay(false);
            if (!progressIsOut) {// 正常
                innerPbBar.setBackgroundColor(getResources().getColor(ProgressBarController.DEFAULT_INNER_BACKGROUND_COLOR_SMALL));
                innerPbBar.setTextColor(getResources().getColor(ProgressBarController.DEFAULT_FONT_COLOR_SMALL));
                externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.CURR_EXTERNAL_COLOR_SMALL));
            } else {// 超出总进度
                innerPbBar.setBackgroundColor(getResources().getColor(ProgressBarController.OUT_INNER_BACKGROUND_COLOR_SMALL));
                innerPbBar.setTextColor(getResources().getColor(ProgressBarController.OUT_FONT_COLOR_BIG));
                externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.OUT_EXTERNAL_COLOR_SMALL));
            }
        }
        reInit();
    }

    /**
     * 重新初始化
     */
    private void reInit() {
        LayoutParams lp_external = new LayoutParams(w_h, w_h);// 166dp
        lp_external.addRule(RelativeLayout.CENTER_IN_PARENT);
        externalPb.setLayoutParams(lp_external);
        strokeWidth = ProgressBarController.BIG_CIRCLE_STROKE / ProgressBarController.BIG_CIRCLE_SCALE * w_h / 2;
        float interval = ProgressBarController.BIG_CIRCLE_INTERVAL / ProgressBarController.BIG_CIRCLE_SCALE * w_h;
        float pbRadius = w_h / 2 - strokeWidth;
        externalPb.setPbRadius(pbRadius);
        //不设左边距和上边距
        externalPb.setInnerLeft(interval);
        externalPb.setInnerTop(interval);
        externalPb.setInterval(interval);
        externalPb.setStrokeWidth(strokeWidth);
        float w_h_inner = 0;
        w_h_inner = ProgressBarController.BIG_CIRCLE_INNER_CIRCLE_SCALE / ProgressBarController.BIG_CIRCLE_SCALE * w_h;

        // 用于伸缩动画
        LayoutParams lp_inner = new LayoutParams((int) w_h_inner, (int) w_h_inner);
        lp_inner.addRule(RelativeLayout.CENTER_IN_PARENT);// 居中 用于伸缩动画
        innerPbBar.setRoundWidth(w_h_inner);
        if (pbIsSmall) {// 小圆(不展示百分比，放大字体大小)
            innerPbBar.setTitle_ts(w_h / ProgressBarController.TITLE_TS_SMALL);
            innerPbBar.setText_u_ts(w_h / ProgressBarController.TEXT_U_TS_SMALL);
        } else {// 大圆
            innerPbBar.setTitle_ts(w_h / ProgressBarController.TITLE_TS_BIG);
            innerPbBar.setText_u_ts(w_h / ProgressBarController.TEXT_U_TS_BIG);
        }
        innerPbBar.setText_ts(w_h / ProgressBarController.TEXT_TS);
        innerPbBar.setPercent_ts(w_h / ProgressBarController.PERCENT_TS);
        innerPbBar.setLayoutParams(lp_inner);

    }

    /**
     * 获取当前进度
     *
     * @param curr
     * @return
     */
    private String getConsumeCircle(float curr) {
        return ((int) curr) + "";
    }

    /**
     * 获取当前进度
     * 用于动画
     *
     * @param curr
     * @return
     */
    private String getConsumeCircleForAnimation(float curr) {
        if (isFloat) {
            return getOdd(curr);
        } else {
            return getConsumeCircle(curr);
        }
    }

    /**
     * 获取当前进度
     * 保留两位小数点
     *
     * @return
     */
    private String getOdd(float curr) {
        DecimalFormat df = new DecimalFormat("###.##");
        return df.format(curr);
    }

    /**
     * 获取外部圆弧进度比
     *
     * @param curr
     * @param max
     * @return
     */
    private float getProgressArcPer(float curr, float max) {
        return curr / max * ProgressBarController.SWEEP_ARC_ANGLE;
    }

    /**
     * 获取内部圆百分比进度
     *
     * @param curr
     * @param max
     * @return
     */
    private String getProgressCircleBaiFenBi(float curr, float max) {
        if (curr < 0) {
            return "0";
        }
        int progress = (int) (curr / max * 100);// 内部圆进度
        return subZeroAndDot(progress + "");
    }

    /**
     * 进度动画
     *
     * @param value    变化值
     * @param duration 持续时间
     */
    private Animator setAnimationPb(int duration, float... value) {
        final float firstValue = value[0];
        final float lastValue = value[1];
        ValueAnimator animator = ValueAnimator.ofFloat(value);
        animator.setDuration(duration);
        animator.setInterpolator(new OvershootInterpolator());
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float value = (Float) animation.getAnimatedValue();
                externalPb.setCurrPb(getProgressArcPer(value, max));
                if (!pbIsSmall) {// 大圆
                    innerPbBar.setProgress(getProgressCircleBaiFenBi(value, max));
                }
                innerPbBar.setText(getConsumeCircleForAnimation(value));
                innerPbBar.invalidate();
                externalPb.invalidate();
            }
        });
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (0 != firstValue) {
                    externalPb.setShowLeftCircleCur(true);
                    externalPb.setShowRightCircleCur(true);
                }
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (lastValue == 0) {
                    externalPb.setShowLeftCircleCur(false);
                    externalPb.setShowRightCircleCur(false);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        return animator;
    }

    /**
     * 设置大圆收缩默认进度动画
     *
     * @param animator
     */
    private void setUpdatePbStatusToBigAnimation(Animator animator) {
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                pbIsSmall = false;
                externalPb.setPbIsSmall(pbIsSmall);
                externalPb.setDefaultPbColor(getResources().getColor(ProgressBarController.DEFAULT_EXTERNAL_COLOR_BIG));
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
    }

    /**
     * 设置大圆收缩默认进度动画
     *
     * @param animator
     */
    private void setShowRightCircleAnimation(Animator animator) {
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                externalPb.setShowRightCircle(false);
                externalPb.setShowRightCircleCur(false);
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
    }

    /**
     * 设置小圆进度转化为大圆当前进度条动画
     *
     * @param animator
     */
    private void setProgressBarColorAnimation(Animator animator) {
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                if (progressIsOut) {// 超出总进度
                    externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.OUT_EXTERNAL_COLOR_BIG));
                } else {
                    externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.CURR_EXTERNAL_COLOR_BIG));
                }
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });

    }

    /**
     * 触发进度动画
     *
     * @param cur      当前进度
     * @param duration 持续时间
     */
    public Animator startPbAnimation(float cur, float max, int duration) {
        this.max = max;
        return setAnimationPb(duration, max, cur);
    }

    /**
     * 小圆收缩动画
     *
     * @param max      总进度
     * @param current  当前进度
     * @param duration 持续时间
     * @param scale    变化值
     * @return
     */
    public Animator startFlexAnimationSmall(float max, float current, int duration, float... scale) {
        this.max = max;
        curr = current;
        ValueAnimator animator = setFlexAnimations(duration, new LinearInterpolator(), scale);
        setFontChangeSmall(animator);
        return animator;
    }

    /**
     * 设置控件伸缩动画
     *
     * @param duration 持续时间
     * @param width    宽度
     * @return
     */
    private ValueAnimator setFlexAnimation(int duration, Interpolator interpolator, int... width) {
        ValueAnimator animator = ValueAnimator.ofInt(width);
        animator.setDuration(duration);
        animator.setInterpolator(interpolator);
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                setWidth(value);
            }
        });
        return animator;
    }

    /**
     * 设置控件伸缩动画
     *
     * @param duration     持续时间
     * @param interpolator 动画频率变化器
     * @param scale        控件尺寸
     * @return
     */
    private ValueAnimator setFlexAnimations(int duration, Interpolator interpolator, float... scale) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(this, "scale", scale);
        animator.setDuration(duration);
        animator.setInterpolator(interpolator);
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float value = (Float) animation.getAnimatedValue();
                ViewHelper.setScaleX(ProgressBarCircle.this, value);
                ViewHelper.setScaleY(ProgressBarCircle.this, value);
            }
        });
        return animator;
    }

    /**
     * 启动大圆伸缩动画
     *
     * @param duration 动画持续时间
     * @param change   是否用详标题
     * @param scale    变化值
     */
    public Animator startFlexAnimationBig(int duration, final boolean change, float... scale) {
        ValueAnimator animator = setFlexAnimations(duration, new AccelerateInterpolator(), scale);
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (!change) {//是否用详标题（原大圆，要变成小圆，单位要缩写）
                    innerPbBar.setProgressIsDisplay(false);
                    innerPbBar.setTxtExpand(true);
                    innerPbBar.setTitle_ts(w_h / 5);
                    innerPbBar.setText_u_ts(w_h / 4 / 2);
                    innerPbBar.setTitle(getSimpleTitle(title));
                    innerPbBar.setText_u(s_u);
                } else {//原小圆,要变成大圆，单位要祥详写
                    innerPbBar.setText_u(unit);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }
        });
        return animator;
    }

    /**
     * 启动颜色变化动画 颜色变完更新圆大小状态 max为－1时，current必须设为－1
     *
     * @param style    颜色动画类型 0:背景颜色动画 1:字体颜色动画
     * @param duration 持续时间
     * @param isChange 是否改变圆大小状态
     * @param color    颜色变化值
     * @return
     */
    public Animator startColorAnimation(int style, int duration, boolean isChange, int... color) {
        this.change = isChange;
        ValueAnimator animator = ValueAnimator.ofInt(color);
        animator.setDuration(duration);
        animator.setEvaluator(new ArgbEvaluator());
        animator.setTarget(innerPbBar);
        if (style == 0) {// 背景颜色动画
            setBackgroundColorAnimation(animator);
        } else if (style == 1) {// 字体颜色动画
            setFontColorAnimation(animator);
        }
        return animator;
    }

    /**
     * 设置字体颜色动画
     *
     * @param animator
     */
    private void setFontColorAnimation(ValueAnimator animator) {
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                innerPbBar.setTextColor(value);
                innerPbBar.invalidate();
            }
        });
    }

    /**
     * 设置背景颜色动画
     *
     * @param animator
     */
    private void setBackgroundColorAnimation(ValueAnimator animator) {
        setBackgroundColorAnimationBig(animator);
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int value = (Integer) animation.getAnimatedValue();
                innerPbBar.setBackgroundColor(value);
                innerPbBar.invalidate();
            }
        });

    }

    /**
     * 设置小圆文字变化动画
     *
     * @param animator
     */
    private void setFontChangeSmall(Animator animator) {
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                innerPbBar.setProgress(getProgressCircleBaiFenBi(curr, max));
                innerPbBar.setTxtExpand(false);
                if (!progressIsOut) {// 正常
                    innerPbBar.setProgressIsDisplay(true);
                }
                innerPbBar.setTitle_ts(w_h / 12);
                innerPbBar.setText_u_ts(w_h / 16);
                innerPbBar.setTitle(getDetailedTitle(title));
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
    }

    /**
     * 设置大圆背景色动画
     *
     * @param animator
     */
    private void setBackgroundColorAnimationBig(Animator animator) {
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (change) {
                    change = false;
                    pbIsSmall = !pbIsSmall;
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
    }

    /**
     * 大圆收缩当前进度动画 或小圆展开当前进度动画
     *
     * @param isSmall  原状态是否是小圆
     * @param duration 持续时间
     * @param txtStart 当前进度变化开始值
     * @param txtEnd   当前进度变化结束值
     * @param value    超出总进度时：外部进度sweep值    正常时：外部进度当前值
     * @return
     */
    public Animator startCurrProgressBarAnimation(boolean isSmall, int duration, float txtStart, float txtEnd, float... value) {
        PropertyValuesHolder pvhE = PropertyValuesHolder.ofFloat("externalPb", value);
        PropertyValuesHolder pvhI = PropertyValuesHolder.ofFloat("innerPbBar", txtStart, txtEnd);
        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(pvhE, pvhI);
        animator.setDuration(duration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float value = (Float) animation.getAnimatedValue("externalPb");
                float text = (Float) animation.getAnimatedValue("innerPbBar");
                if (progressIsOut) {
                    externalPb.setCurrPb(value);
                } else {
                    externalPb.setCurrPb(getProgressArcPer(value, max));
                }
                externalPb.invalidate();
                if (!pbIsSmall) {// 大圆
                    innerPbBar.setProgress(getProgressCircleBaiFenBi(text, max));
                }
                innerPbBar.setText(getConsumeCircleForAnimation(text));
                innerPbBar.invalidate();
            }
        });
        if (isSmall) {
            setProgressBarColorAnimation(animator);
        }
        return animator;
    }

    /**
     * 大圆收缩后默认进度动画 或小圆展开后默认进度动画
     *
     * @param isBig      原大小是否大圆
     * @param sweepStart 外部进度变化的开始角度
     * @param sweepEnd   外部进度变化的结束角度
     * @return
     */
    public Animator startDefaultProgressBarAnimation(boolean isBig, int duration, int sweepStart, int sweepEnd) {
        PropertyValuesHolder pvhS = PropertyValuesHolder.ofInt("sweep", sweepStart, sweepEnd);
        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(pvhS);
        animator.setDuration(duration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int sweep = (Integer) animation.getAnimatedValue("sweep");
                externalPb.setDefaultSweep(sweep);
                externalPb.invalidate();
            }
        });

        if (isBig) {
            setShowRightCircleAnimation(animator);
        } else {
            setUpdatePbStatusToBigAnimation(animator);
        }
        return animator;
    }

    /**
     * 默认和当前进度合一为满圆动画
     *
     * @param duration
     * @param textStart 当前进度变化开始值
     * @param textEnd   当前进度变化结束值
     * @param value     外部圆弧进度sweep变化值
     * @return
     */
    public Animator startCircleProgressBarAnimation(int duration, float textStart, float textEnd, float... value) {
        PropertyValuesHolder pvhE = PropertyValuesHolder.ofFloat("externalPb", value);
        PropertyValuesHolder pvhI = PropertyValuesHolder.ofFloat("innerPbBar", textStart, textEnd);
        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(pvhE, pvhI);
        animator.setDuration(duration);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                Float value = (Float) animation.getAnimatedValue("externalPb");
                float text = (Float) animation.getAnimatedValue("innerPbBar");
                externalPb.setCurrPb(value);
                innerPbBar.setText(getConsumeCircleForAnimation(text));
                externalPb.invalidate();
                innerPbBar.invalidate();
            }
        });
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                externalPb.setPbIsSmall(true);
                if (progressIsOut) {// 超出总进度
                    externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.OUT_INNER_BACKGROUND_COLOR_SMALL));
                } else {
                    externalPb.setCurrPbColor(getResources().getColor(ProgressBarController.DEFAULT_INNER_BACKGROUND_COLOR_SMALL));
                }
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                externalPb.setShowRightCircle(true);// 上个动画设为false，现在重置回来
                externalPb.setShowRightCircleCur(true);// 上个动画设为false，现在重置回来
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }
        });
        return animator;
    }

    /**
     * 大圆位移动画（小圆位移动画） 小圆位置－大圆变小圆的差值－大圆的位置
     *
     * @param translationX 小圆到父控件左位置－大圆变小圆的差值-变化后的小圆位置
     * @param translationY 小圆到父控件上位置－大圆变小圆的差值
     * @return
     */
    public Animator startTranslateAnimation(float translationX, float translationY) {
        PropertyValuesHolder pvhX = PropertyValuesHolder.ofFloat("translationX", translationX);
        PropertyValuesHolder pvhY = PropertyValuesHolder.ofFloat("translationY", translationY);
        ValueAnimator animator = ValueAnimator.ofPropertyValuesHolder(pvhX, pvhY);
        animator.addListener(new AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (null != listener) {
                    listener.recover(ProgressBarCircle.this);
                    listener = null;
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animator.addUpdateListener(new AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                float tx = (Float) animation.getAnimatedValue("translationX");
                ViewHelper.setTranslationX(ProgressBarCircle.this, tx);
                float ty = (Float) animation.getAnimatedValue("translationY");
                ViewHelper.setTranslationY(ProgressBarCircle.this, ty);
            }
        });
        return animator;
    }

    /**
     * 设置控件宽度并重新初始化
     *
     * @param w
     */
    public void setWidth(int w) {
        w_h = w;
        reInit();
    }

    /**
     * 获取超出进度状态
     *
     * @return
     */
    public boolean getProgressIsOut() {
        return progressIsOut;
    }

    /**
     * 获取内部圆背景色
     *
     * @return
     */
    public int getInnerPbBackGroundColor() {
        return innerPbBar.getBackgroundColor();
    }

    /**
     * 获取内部圆文字颜色
     *
     * @return
     */
    public int getInnerFontColor() {
        return innerPbBar.getFontColor();
    }

    /**
     * 设置进度监听器
     *
     * @param listener
     */
    public void setProgressBarListener(ProgressBarListener listener) {
        this.listener = listener;
    }

    /**
     * 设置控件大小状态
     *
     * @param pbIsSmall
     */
    public void setPbIsSmall(boolean pbIsSmall) {
        this.pbIsSmall = pbIsSmall;
    }

    /**
     * 重置现有状态
     *
     * @param small 现状态是否是小圆
     */
//    public void recover(boolean small) {
//        if (small) {
//            pbIsSmall = true;
////            innerPbBar.setPbIsSmall(true);
//            externalPb.setPbIsSmall(true);
//        } else {
//            pbIsSmall = false;
////            innerPbBar.setPbIsSmall(false);
//            externalPb.setPbIsSmall(false);
//        }
//    }

    /**
     * 获取宽度
     *
     * @return
     */
    public int getW_h() {
        return w_h;
    }

    /**
     * 设置支持浮点
     */
    public void setIsFloat(boolean isFloat) {
        this.isFloat = isFloat;
    }

}
