package com.example.circleanimation;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

/**
 * 目标跳转页面
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class DemoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.demo);
        TextView skip_tv = (TextView) findViewById(R.id.skip_tv);
        String content = getIntent().getStringExtra("content");
        skip_tv.setText(content);
    }
}
