package com.example.circleanimation.util;

import android.util.Log;

/**
 * 公用日志
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class LogEx {

    private static boolean isOpen = true;

    public static void d(String tag, String msg) {
        if (isOpen) {
            Log.d(tag, isNull(msg));
        }
    }

    public static void i(String tag, String msg) {
        if (isOpen) {
            Log.i(tag, isNull(msg));
        }
    }

    public static void e(String tag, String msg) {
        if (isOpen) {
            Log.e(tag, isNull(msg));
        }
    }

    private static String isNull(String s) {
        if (s == null) {
            return "null";
        } else {
            return s;
        }
    }

}
