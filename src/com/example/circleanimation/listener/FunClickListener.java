package com.example.circleanimation.listener;

import com.example.circleanimation.model.PBCInfo;

/**
 * 功能按钮点击事件监听器
 * Author：cjj
 * Blog：jianjunchen.net
 */
public interface FunClickListener {
    void left_fun_click();//左功能健的点击事件

    void right_fun_click();//右功能健的点击事件

    void big_circle_click(PBCInfo pbcInfo);//大圆的点击事件

}
