package com.example.circleanimation.model;

import com.example.circleanimation.widget.ProgressBarCircle;
import com.nineoldandroids.animation.Animator;

/**
 * 小圆点击事件数据
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class PBCClickInfo {
    private float maxSmall = 0;//小圆进度最大值
    private float currSmall = 0;//小圆当前进度
    private int widthSmall = 0;//小圆宽度值
    private Animator animatorFlexSmall = null;//小圆收缩动画
    private ProgressBarCircle pbcSmall = null;//小圆

    public void setAnimatorFlexSmall(Animator animatorFlexSmall) {
        this.animatorFlexSmall = animatorFlexSmall;
    }

    public Animator getAnimatorFlexLeft() {
        return animatorFlexSmall;
    }

    public float getCurrSmall() {
        return currSmall;
    }

    public void setCurrSmall(float currSmall) {
        this.currSmall = currSmall;
    }

    public float getMaxSmall() {
        return maxSmall;
    }

    public void setMaxSmall(float maxSmall) {
        this.maxSmall = maxSmall;
    }

    public ProgressBarCircle getPbcSmall() {
        return pbcSmall;
    }

    public void setPbcSmall(ProgressBarCircle pbcSmall) {
        this.pbcSmall = pbcSmall;
    }

    public int getWidthSmall() {
        return widthSmall;
    }

    public void setWidthSmall(int widthSmall) {
        this.widthSmall = widthSmall;
    }
}
