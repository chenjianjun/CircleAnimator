package com.example.circleanimation.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.util.AttributeSet;
import android.view.View;

import com.example.circleanimation.R;


/**
 * 进度内部圆
 * Author：cjj
 * Blog：jianjunchen.net
 */
public class InnerRoundProgressBar extends View {
    private static final String TAG = "InnerRoundProgressBar";

    private Paint paint;
    //内部圆颜色，标题颜色，内容颜色，百分比颜色
    private int roundColor, title_tc, text_tc, percent_tc;
    private float roundWidth;//内部圆宽度
    //标题大小，内容大小，内容单位的文字大小，百分比文字大小
    private float title_ts, text_ts, text_u_ts, percent_ts;
    private String progress;//进度百分比
    private boolean progressIsDisplay;//是否展示百分比，默认展示:true
    //标题，内容，内容单位
    private String title, text, text_u;
    /**
     * 是否缩小了
     */
//	private boolean pbIsSmall = false;//是否小圆
    private boolean txtExpand = false;//文字间（标题和内容间）是否拉大距离
//	private float circleCenter = 0;//圆心

    public InnerRoundProgressBar(Context context) {
        this(context, null);
    }

    public InnerRoundProgressBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InnerRoundProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.InnerRoundProgressBar);

        roundColor = ta.getColor(R.styleable.InnerRoundProgressBar_roundColor, getResources().getColor(R.color._fe9602));
        title_tc = ta.getColor(R.styleable.InnerRoundProgressBar_title_tc, getResources().getColor(R.color._ffffff));
        text_tc = ta.getColor(R.styleable.InnerRoundProgressBar_text_tc, getResources().getColor(R.color._ffffff));
        percent_tc = ta.getColor(R.styleable.InnerRoundProgressBar_percent_tc, getResources().getColor(R.color._ffffff));

        roundWidth = ta.getDimension(R.styleable.InnerRoundProgressBar_roundWidth, 0);
        title_ts = ta.getDimension(R.styleable.InnerRoundProgressBar_title_ts, 0);
        text_ts = ta.getDimension(R.styleable.InnerRoundProgressBar_text_ts, 0);
        text_u_ts = ta.getDimension(R.styleable.InnerRoundProgressBar_text_u_ts, 0);
        percent_ts = ta.getDimension(R.styleable.InnerRoundProgressBar_percent_ts, 0);

        progressIsDisplay = ta.getBoolean(R.styleable.InnerRoundProgressBar_progressIsDisplay, true);

        progress = ta.getString(R.styleable.InnerRoundProgressBar_progress);
        if (null == progress) {
            progress = "百分比";
        }
        title = ta.getString(R.styleable.InnerRoundProgressBar_title);
        if (null == title) {
            title = "标题";
        }
        text = ta.getString(R.styleable.InnerRoundProgressBar_text);
        if (null == text) {
            text = "内容";
        }
        text_u = ta.getString(R.styleable.InnerRoundProgressBar_text_u);
        if (null == text_u) {
            text_u = "单位";
        }
        ta.recycle();
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
//		LogEx.d(TAG,"draw");
        try {
            float radius = roundWidth / 2;//半径
            float y_wuFenYi = roundWidth / 5;//圆的五分之一的y坐标
            float y_wuFenSi = roundWidth * 4 / 5;//圆的五分之四的y坐标

            paint = new Paint();
            paint.setColor(roundColor);
            paint.setStyle(Style.FILL);//实心圆
            paint.setAntiAlias(true);//去锯齿
            canvas.drawCircle(radius, radius, radius, paint);//画圆

            //画标题文字  圆的1/3
            paint.setColor(title_tc);
            paint.setTextSize(title_ts);
            paint.setTextAlign(Align.CENTER);//文字中间的坐标为参考
            canvas.drawText(title, radius, y_wuFenYi + title_ts, paint);//x：由于以文字中间坐标参考，故取圆心即可居中

            float y_consumed = 0;//内容的y坐标
            float y_consumed_unit = 0;//内容单位的y坐标
            if (txtExpand) {//小圆(不展示百分比,拉开距离)
                y_consumed = radius * 4 / 3 + text_ts * 5 / 12;
                y_consumed_unit = radius * 4 / 3 - text_ts * 5 / 12 + text_u_ts;
            } else {//大圆(展示百分比)
                y_consumed = radius + text_ts * 5 / 12;
                y_consumed_unit = radius - text_ts * 4 / 12 + text_u_ts;
            }
            //画内容文字  大圆：圆的中间  小圆：圆的2/3
            paint.setColor(text_tc);
            paint.setTextSize(text_ts);
            paint.setAntiAlias(true);//去锯齿
            paint.setTextAlign(Align.LEFT);//文字左边的坐标为参考
            Paint p = new Paint();
            p.setTextSize(text_u_ts);
            float unitWidth = p.measureText(text_u);
            float textMiddleWidth = paint.measureText(text) + unitWidth;//测量文字长度，用作算出文字开始偏移量(居中)
            canvas.drawText(text, radius - textMiddleWidth / 2, y_consumed, paint);//参考下面
            //画内容文字的单位     大圆：圆的中间  小圆：圆的2/3
            paint.setTextAlign(Align.LEFT);//文字左边的坐标为参考
            paint.setColor(text_tc);
            paint.setTextSize(text_u_ts);
            canvas.drawText(text_u, radius + textMiddleWidth / 2 - unitWidth, y_consumed_unit, paint);//x：内容的最右边x坐标  y:针对画数字时存在六分之一的顶部间隔故圆心x坐标-（内容高度-六分之一内容高度）/2+内容单位高度

            if (progressIsDisplay) {
                //画进度百分比
                paint.setColor(percent_tc);
                paint.setTextSize(percent_ts);
                paint.setTextAlign(Align.CENTER);//文字左边的坐标为参考
                canvas.drawText(progress + "%", radius, y_wuFenSi, paint);//x:左边距+x点到圆左方距离
//			paint = new Paint();
//			paint.setColor(percent_tc);
//			paint.setTextSize(percent_ts);
//			paint.setAntiAlias(true);//去锯齿
//			paint.setTextAlign(Align.LEFT);//文字左边的坐标为参考
//			float x_side = (float)(Math.sqrt(Math.pow(radius, 2)-Math.pow(y_sanFenEr+percent_ts-y_erFenYi, 2)));//勾股定理求出x到圆心的距离
//			canvas.drawText(progress+"%", y_top+radius-x_side+percent_ts, y_wuFenSi, paint);//x:左边距+x点到圆左方距离

                //画百分比单位
//			paint = new Paint();
//			paint.setColor(textColorBottomRight);
//			paint.setAntiAlias(true);//去锯齿
//			paint.setTextSize(textSizeBottomRightUnit);
//			paint.setTextAlign(Align.RIGHT);//文字右边的坐标为参考
//			float x_bru = y_top+radius+x_side;//百分比的单位的x坐标起始点(计算时为避免与圆有交叉需减去字体大小)
//			canvas.drawText(textUnitBottomRight,x_bru-textSizeBottomRightUnit ,y_wuFenSi-textSizeBottomRightUnit, paint);//x:为避免与圆交叉，减去textSizeBottomRightUnit
//			//画百分比
//			paint = new Paint();
//			paint.setTextAlign(Align.RIGHT);//文字右边的坐标为参考
//			paint.setColor(textColorBottomRight);
//			paint.setAntiAlias(true);//去锯齿
//			paint.setTextSize(textSizeBottomRight);
//			float txtBottomRightUnitWidth = paint.measureText(textUnitBottomRight);
//			canvas.drawText(textBottomRight, x_bru-txtBottomRightUnitWidth,y_wuFenSi, paint);//参考上
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 设置标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 设置内容
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * 获取内容的单位
     *
     * @return
     */
    public String getText_u() {
        return text_u;
    }

    /**
     * 设置内容的单位
     */
    public void setText_u(String text_u) {
        this.text_u = text_u;
    }

    /**
     * 设置标题的颜色
     */
    public void setTitle_tc(int title_tc) {
        this.title_tc = title_tc;
    }

    /**
     * 设置内容的颜色
     */
    public void setText_tc(int text_tc) {
        this.text_tc = text_tc;
    }

    /**
     * 设置百分比颜色
     */
    public void setPercent_tc(int percent_tc) {
        this.percent_tc = percent_tc;
    }

    /**
     * 设置标题大小
     */
    public void setTitle_ts(int title_ts) {
        this.title_ts = title_ts;
    }

    /**
     * 设置内容大小
     */
    public void setText_ts(int text_ts) {
        this.text_ts = text_ts;
    }

    /**
     * 设置内容单位大小
     */
    public void setText_u_ts(int text_u_ts) {
        this.text_u_ts = text_u_ts;
    }

    /**
     * 设置百分比大小
     */
    public void setPercent_ts(int percent_ts) {
        this.percent_ts = percent_ts;
    }

    /**
     * 设置圆宽
     *
     * @param roundWidth
     */
    public void setRoundWidth(float roundWidth) {
        this.roundWidth = roundWidth;
    }

    /**
     * 获取圆宽
     */
//	public float getRoundWidth(){
//		return roundWidth;
//	}

    /**
     * 设置进度
     *
     * @param progress
     */
    public void setProgress(String progress) {
        this.progress = progress;
    }

    /**
     * 设置内部所有文字的颜色
     */
    public void setTextColor(int textColor) {
        setTitle_tc(textColor);
        setText_tc(textColor);
        setPercent_tc(textColor);
    }

    /**
     * 是否展示底部百分比进度
     *
     * @param progressIsDisplay
     */
    public void setProgressIsDisplay(boolean progressIsDisplay) {
        this.progressIsDisplay = progressIsDisplay;
    }

    /**
     * 设置圆大小 状态
     * @param pbIsSmall
     */
//	public void setPbIsSmall(boolean pbIsSmall) {
//		this.pbIsSmall = pbIsSmall;
//	}

    /**
     * 获取圆背景颜色
     */
    public int getBackgroundColor() {
        return roundColor;
    }

    /**
     * 设置圆背景颜色
     *
     * @param roundColor
     */
    public void setBackgroundColor(int roundColor) {
        this.roundColor = roundColor;
    }

    /**
     * 获取文字颜色
     *
     * @return
     */
    public int getFontColor() {
        return title_tc;
    }

    /**
     * 设置文字间的距离
     *
     * @param txtExpand
     */
    public void setTxtExpand(boolean txtExpand) {
        this.txtExpand = txtExpand;
    }

    /**
     * 设置圆心
     * @param circleCenter
     */
//	public void setCircleCenter(float circleCenter) {
//		this.circleCenter = circleCenter;
//	}

}
